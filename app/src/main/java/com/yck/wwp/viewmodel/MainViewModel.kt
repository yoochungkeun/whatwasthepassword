package com.yck.wwp.viewmodel
import com.yck.wwp.model.ViewData
import com.yck.wwp.view.fragment.FragmentTypeFavorite
import com.yck.wwp.view.fragment.FragmentTypeMain

class MainViewModel() {

    var mainFrag = FragmentTypeMain()
    var favoriteFrag = FragmentTypeFavorite()

    fun getMainItem(keyword:String){
       mainFrag.getItems(keyword)
    }

    fun getFavoriteItem(keyword:String){
       favoriteFrag.getItems(keyword)
    }

    fun removeMainItem(viewdata:ViewData,itemIdx:Int){
        mainFrag.removeItems(viewdata,itemIdx)
    }

    fun removeFavoriteItem(viewdata:ViewData,itemIdx:Int){
        favoriteFrag.removeItems(viewdata,itemIdx)
    }

}//class end