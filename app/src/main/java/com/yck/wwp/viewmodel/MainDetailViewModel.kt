package com.yck.wwp.viewmodel

import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import com.yck.wwp.base.COMMAND_ADD_COMPLETE
import com.yck.wwp.base.COMMAND_MODIFY_COMPLETE
import com.yck.wwp.base.PUTEXTRA_KEY
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ViewData

class MainDetailViewModel {

    val mMainDetailLiveData = MutableLiveData<ViewData>()
    var mViewdata = ViewData()

    fun getIntentData(intent:Intent){
        if(intent.hasExtra(PUTEXTRA_KEY)){
            if(intent.getSerializableExtra(PUTEXTRA_KEY) is ViewData){
                mViewdata = intent.getSerializableExtra(PUTEXTRA_KEY) as ViewData
            }else{
                mViewdata.command = intent.getStringExtra(PUTEXTRA_KEY).toString()
            }
        }
        mMainDetailLiveData.postValue(mViewdata)
    }

    fun addDBItems(viewData: ViewData) {
        GlobalScope.run {
            DIManager.dbUtils.initDB()
            DIManager.dbUtils.addItem(viewData.item)
        }
        mMainDetailLiveData.postValue(ViewData(command = COMMAND_ADD_COMPLETE,item = viewData.item))
    }

    fun updateDBItems(viewData: ViewData) {
        GlobalScope.run {
            DIManager.dbUtils.initDB()
            DIManager.dbUtils.updateItem(viewData.item)
        }
        mMainDetailLiveData.postValue(ViewData(command = COMMAND_MODIFY_COMPLETE,item = viewData.item))
    }


}//class end