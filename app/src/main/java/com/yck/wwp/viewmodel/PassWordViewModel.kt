package com.yck.wwp.viewmodel

import androidx.lifecycle.MutableLiveData
import timber.log.Timber
import com.yck.wwp.base.*
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ViewData

class PassWordViewModel {

    val mPasswordLiveData = MutableLiveData<ViewData>()
    var mViewData = ViewData()
    var mTempRegisterInput = ""


    //비번입력 상태따른 분기처리
    fun passProcess(viewdata:ViewData){
        if(viewdata==null){
            return
        }
        mViewData = viewdata
        when(mViewData.pass_command){
            COMMAND_PASS_REGISTER -> {
                passRegister()
            }
            COMMAND_PASS_CONFIRM -> {
                passConfirm()
            }
            COMMAND_PASS_CHANGE -> {
                passChange()
            }
            COMMAND_PASS_UNLOCK -> {
                passUnlock()
            }
            COMMAND_PASS_REGISTER_RE -> {
                passRegisterRE()
            }
            else ->{
                //None
            }
        }
    }

    //비번확인
    fun passConfirm(){
        //비번 확인
        if(mViewData.inputPass.isNullOrEmpty()){
            mViewData.pass_command = COMMAND_PASS_CONFIRM_FAILED
            return
        }
        Timber.e("PREF_SAVED_PASS_KEY : ${DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY)}")
        Timber.e("mViewData.inputPass : ${mViewData.inputPass}")

        var decryptPassword = DIManager.convertUtils.convertDeCryptString(DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY))
        Timber.e("decryptPassword : ${decryptPassword}")

        if(decryptPassword.equals(mViewData.inputPass)){
            mViewData.pass_command = COMMAND_PASS_CONFIRM_SUCCESS
        }else{
            mViewData.pass_command = COMMAND_PASS_CONFIRM_FAILED
        }
        mPasswordLiveData.postValue(mViewData)
    }

    //비번 등록
    fun passRegister(){
        if(mViewData.inputPass.isNullOrEmpty()){
            mViewData.pass_command = COMMAND_PASS_REGISTER_FAILED
            return
        }
        mViewData.pass_command = COMMAND_PASS_REGISTER_SUCCESS
        mTempRegisterInput = mViewData.inputPass
        mPasswordLiveData.postValue(mViewData)
    }

    //비번 변경
    fun passChange(){
        if(mViewData.inputPass.isNullOrEmpty()){
            mViewData.pass_command = COMMAND_PASS_CHANGE_FAILED
            return
        }
        var decryptPassword = DIManager.convertUtils.convertDeCryptString(DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY))
        if(decryptPassword.equals(mViewData.inputPass)){
            mViewData.pass_command = COMMAND_PASS_CHANGE_SUCCESS
        }else{
            mViewData.pass_command = COMMAND_PASS_CHANGE_FAILED
        }
        mPasswordLiveData.postValue(mViewData)
    }


    //비번 재등록
    fun passRegisterRE(){

        if(mViewData.inputPass.isNullOrEmpty()){
            mViewData.pass_command = COMMAND_PASS_REGISTER_RE_FAILED
            return
        }
        //Register 입력한 비번과 일치하는지 확인
        if(mViewData.inputPass.equals(mTempRegisterInput)){
            //일치하면 비밀번호 등록 완료
            var encryptPassword = DIManager.convertUtils.convertEnCryptString(mViewData.inputPass)
            DIManager.repoSharedPref.setStringPref(PREF_SAVED_PASS_KEY,encryptPassword)
            mViewData.pass_command = COMMAND_PASS_REGISTER_RE_SUCCESS
            mPasswordLiveData.postValue(mViewData)
            return
        }
        mViewData.pass_command = COMMAND_PASS_REGISTER_RE_FAILED
        mPasswordLiveData.postValue(mViewData)
    }

    //비번 해제
    fun passUnlock(){
        //비번 확인
        if(mViewData.inputPass.isNullOrEmpty()){
            mViewData.pass_command = COMMAND_PASS_UNLOCK_FAILED
            return
        }
        var decryptPassword = DIManager.convertUtils.convertDeCryptString(DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY))
        if(decryptPassword.equals(mViewData.inputPass)){
            DIManager.repoSharedPref.setStringPref(PREF_SAVED_PASS_KEY,"")
            mViewData.pass_command = COMMAND_PASS_UNLOCK_SUCCESS
        }else{
            mViewData.pass_command = COMMAND_PASS_UNLOCK_FAILED
        }
        mPasswordLiveData.postValue(mViewData)
    }


}//class end