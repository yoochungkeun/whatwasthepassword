package com.yck.wwp.viewmodel

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData

class FragFavoriteViewModel {

    val mFavoriteFragLiveData = MutableLiveData<ViewData>()

    fun removeDBItems(viewData:ViewData,itemIdx:Int) {
        GlobalScope.run {
            DIManager.dbUtils.initDB()
            DIManager.dbUtils.deleteItem(itemIdx)
        }
        var itemList = DIManager.dbUtils.getAllItems()
        itemList.let {
            mFavoriteFragLiveData.postValue(ViewData(command = viewData.command,itemList = itemList))
        }
    }

    fun getItemsFavorite(keyword:String,isFavorite:String){
        var itemList:MutableList<ItemsData>
        GlobalScope.run {
            DIManager.dbUtils.initDB()
            itemList = DIManager.dbUtils.searchIteminFavorite(keyword,isFavorite)
        }
        mFavoriteFragLiveData.postValue(ViewData(itemList = itemList))
    }

    fun getIdDBItems(command:String,idx:Int){
        var item:ItemsData
        GlobalScope.run {
            DIManager.dbUtils.initDB()
            item = DIManager.dbUtils.getIdItem(idx)
        }
        item.let {
            mFavoriteFragLiveData.postValue(ViewData(command = command,item = item))
        }
    }

}//class end