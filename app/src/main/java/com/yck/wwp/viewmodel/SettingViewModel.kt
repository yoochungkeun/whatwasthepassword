package com.yck.wwp.viewmodel

import androidx.lifecycle.MutableLiveData
import com.yck.wwp.base.COMMAND_REMOVE_DB_COMPLETE
import com.yck.wwp.base.PREF_SAVED_GRID_TYPE_KEY
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData
import com.yck.wwp.utils.DBimportExportUitls
import kotlinx.coroutines.GlobalScope
import timber.log.Timber

class SettingViewModel {

    val mSettingLiveData = MutableLiveData<ViewData>()

    //전체 데이터 가져오기
    fun getAllDBItemsForCreateFile(viewdata:ViewData){
        var itemList:MutableList<ItemsData>
        GlobalScope.run {
            DIManager.dbUtils.initDB()
            itemList = DIManager.dbUtils.getAllItems()
        }
        mSettingLiveData.postValue(ViewData(command = viewdata.command ,itemList = itemList))
    }

    //저장 리스트 백업 하기
    fun createDataFile(db_backup:DBimportExportUitls,viewdata: ViewData) {
        Timber.e("viewdata.itemList : ${viewdata.itemList}")
        GlobalScope.run{
            db_backup.startExcelWork(viewdata.itemList)
            db_backup.createExcelFile()
        }
        mSettingLiveData.postValue(ViewData(command = viewdata.command))
    }

    //데이터 불러오기
    fun importBackupFile(db_backup:DBimportExportUitls,path:String,viewdata: ViewData){
        GlobalScope.run{
            db_backup.importBackupFile(path)
        }
        mSettingLiveData.postValue(ViewData(command = viewdata.command))
    }

    //정렬방식 변경 유무 확인값
    fun isChangedGridType(mGridTypeCountNum:Int):Boolean{
        if (DIManager.repoSharedPref.getIntPref(PREF_SAVED_GRID_TYPE_KEY) != mGridTypeCountNum) {
            DIManager.repoSharedPref.setIntPref(PREF_SAVED_GRID_TYPE_KEY, mGridTypeCountNum)
            return true
        }
        return false
    }

    //DB 초기화
    fun clearDB(viewdata: ViewData){
        GlobalScope.run {
            DIManager.dbUtils.initDB()
            DIManager.dbUtils.clearDBData()
        }
        viewdata.command = COMMAND_REMOVE_DB_COMPLETE
        mSettingLiveData.postValue(ViewData(command = viewdata.command))
    }


}//class end