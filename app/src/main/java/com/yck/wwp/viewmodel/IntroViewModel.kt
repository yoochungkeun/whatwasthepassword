package com.yck.wwp.viewmodel

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.yck.wwp.R
import com.yck.wwp.di.DIManager
import com.yck.wwp.dialog.CommonDialog
import com.yck.wwp.interfaces.IDialogClickListener
import com.yck.wwp.interfaces.IIntroBase
import com.yck.wwp.interfaces.IIntroBase.view
import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData
import com.yck.wwp.utils.FirebaseUtils
import com.yck.wwp.view.activity.ActivityMain


class IntroViewModel(activity: Activity):IIntroBase.viewModel{

    val mActivity = activity
    val mIntroLiveData = MutableLiveData<ViewData>()

    override fun openActivitiyMain(activity: Activity) {
        mActivity.startActivity(Intent(mActivity, activity::class.java))
        mActivity.overridePendingTransition(0, R.anim.fade_out)
        mActivity.finish()
    }

    override fun getFirebaseCloudStoreData(){
        DIManager.firebaseUtils.startGetFirebseCloudStoreData(mIntroLiveData)
    }

}//class endaa