package com.yck.wwp.base

import android.app.Activity
import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.yck.wwp.R
import com.yck.wwp.global.GlobalData
import timber.log.Timber


open class BaseActivity :AppCompatActivity(),LifecycleObserver{

    lateinit var mBaseActivity:Activity
    lateinit var mBaseContext:Context
    var mBackKeyDoubleClickDelayTime:Long = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        Timber.plant(Timber.DebugTree())
        mBaseActivity = this
        mBaseContext = this
    }

    override fun onDestroy() {
        super.onDestroy()
        ProcessLifecycleOwner.get().lifecycle.removeObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onBasePause(){
        Timber.e("onBasePause")
    }

    fun startActivityAnimation(){
        overridePendingTransition(R.anim.start_slide_to_left_in, R.anim.start_slide_to_right_out)
    }

    fun finishActivityAnimation(){
        overridePendingTransition(R.anim.finish_slide_to_right_in, R.anim.finish_slide_to_left_out)
    }

    fun setStatusBar(statusbarColr: Int){
        if (Build.VERSION.SDK_INT >= 21) {
            val window: Window = this.window
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.setStatusBarColor(statusbarColr)
        }
    }

    fun finishProcessCancel(){
        setResult(RESULT_CANCELED)
        finish()
        finishActivityAnimation()
    }

    fun doubleClickFinishApp(){
        if (System.currentTimeMillis() > mBackKeyDoubleClickDelayTime+2000){
            Toast.makeText(this, resources.getString(R.string.str_double_click_finish), Toast.LENGTH_SHORT).show()
            mBackKeyDoubleClickDelayTime = System.currentTimeMillis()
        }else{
            finish()
        }
    }

    fun addActivity(activity: Activity){
        if(activity!=null){
            GlobalData.comp_activityList.add(activity)
        }
    }

    //앱 완전 종료
    fun finishApp(){
        if(GlobalData.comp_activityList!=null&&GlobalData.comp_activityList.size>0){
            for(i in 0 until GlobalData.comp_activityList.size step 1){
                GlobalData.comp_activityList.get(i).finish()
            }
        }
    }

    fun disabelPopupWindow(){
        var popupview = View.inflate(this,R.layout.dialog_layout, null)
        var popup = PopupWindow(popupview, 200, 100, true)
        popup.setOutsideTouchable(true)
        popup.setBackgroundDrawable(BitmapDrawable())
    }

}//class end