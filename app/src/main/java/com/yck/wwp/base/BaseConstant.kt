package com.yck.wwp.base

const val DB_RM_CONTACTS = "database-rm_contacts"

const val COMMAND_MODIFY = "command_modify"
const val COMMAND_MODIFY_COMPLETE = "command_modify_complete"
const val COMMAND_ADD_COMPLETE = "command_addComplete"
const val COMMAND_ADD ="command_add"
const val COMMAND_DELETE ="command_delete"
const val COMMAND_CLICK_ITEM ="command_click_item"
const val COMMAND_BACKUP_COMPLETE ="command_backup_complete"
const val COMMAND_BACKUP_IMPORT_COMPLETE ="command_backup_import_complete"
const val COMMAND_BACKUP ="command_backup"
const val COMMAND_BACKUP_IMPORT ="command_backup_import"
const val COMMAND_REMOVE_DB ="command_remove_db"
const val COMMAND_REMOVE_DB_COMPLETE ="command_remove_db_complete"


//비번등록
const val COMMAND_PASS_REGISTER ="command_register"
const val COMMAND_PASS_REGISTER_SUCCESS ="command_register_success"
const val COMMAND_PASS_REGISTER_FAILED ="command_register_failed"

//비번확인
const val COMMAND_PASS_CONFIRM ="command_confirm"
const val COMMAND_PASS_CONFIRM_SUCCESS ="command_confirm_success"
const val COMMAND_PASS_CONFIRM_FAILED ="command_confirm_failed"

//비번 재등록
const val COMMAND_PASS_REGISTER_RE ="command_register_re"
const val COMMAND_PASS_REGISTER_RE_SUCCESS ="command_register_re_success"
const val COMMAND_PASS_REGISTER_RE_FAILED ="command_register_re_failed"

//비번해제
const val COMMAND_PASS_UNLOCK ="command_pass_unlock"
const val COMMAND_PASS_UNLOCK_SUCCESS ="command_unlock_success"
const val COMMAND_PASS_UNLOCK_FAILED ="command_unlock_failed"

//비번변경
const val COMMAND_PASS_CHANGE ="command_pass_change"
const val COMMAND_PASS_CHANGE_SUCCESS ="command_change_success"
const val COMMAND_PASS_CHANGE_FAILED ="command_change_failed"

//SHARED PREF
const val PREF_BASE_PREFNAME = "jikimipref"
const val PREF_SAVED_PASS_KEY = "pref_saved_pass_key"
const val PREF_SAVED_FILE_PATH_KEY = "pref_saved_file_path_key"
const val PREF_SAVED_GRID_TYPE_KEY = "pref_saved_grid_type_key"
const val PREF_SAVED_INTRO_FIREBASE_CLOUDSTORE_KEY = "pref_saved_intro_firebase_cloudstore_key"

//GRID_TYPE
const val RECYCLERVIEW_ITEM_GRID_TYPE_1 = 1
const val RECYCLERVIEW_ITEM_GRID_TYPE_2 = 2
const val RECYCLERVIEW_ITEM_GRID_TYPE_3 = 3

//PUT EXTRAS
const val PUTEXTRA_KEY="putExtra_key"
const val SUCCESS = "success"

const val TAB_MAX_ITEM_COUNT = 2
const val TAB_MAIN_FRAG_PAGE = 0
const val TAB_FAVORITE_FRAG_PAGE = 1

const val FAVORITE_TRUE = "true"
const val FAVORITE_FALSE = "false"
const val FAVORITE_X = "x"
const val FAVORITE_O = "o"

const val BUISINESS_EXCEL_FILE_NAME = "bizi_book"

const val CLIP_BOARD_LABEL = "BIZI_CLIPBOARD_LABEL"

const val FIREBASE_CLOUDSTORE_COLLECTION = "bizi"
const val FIREBASE_CLOUDSTORE_DOCUMENT = "intro"
const val FIREBASE_CLOUDSTORE_COMPLETE = "get_firebasedata_complete"