package com.yck.wwp.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.yck.wwp.R

open class BaseFragment:Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    fun startActivityAnimation(){
        requireActivity().overridePendingTransition(R.anim.start_slide_to_left_in, R.anim.start_slide_to_right_out)
    }

    fun finishActivityAnimation(){
        requireActivity().overridePendingTransition(R.anim.finish_slide_to_right_in, R.anim.finish_slide_to_left_out)
        requireActivity().finish()
    }


}//class end