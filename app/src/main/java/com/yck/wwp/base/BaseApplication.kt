package com.yck.wwp.base

import android.app.Application
import ck.accountlistmanager.di.customDImodules
import org.koin.android.ext.android.startKoin

class BaseApplication:Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(applicationContext,
            customDImodules
        )
    }

}//class end