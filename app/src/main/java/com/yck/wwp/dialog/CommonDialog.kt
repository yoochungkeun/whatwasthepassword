package com.yck.wwp.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Html
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.di.DIManager
import com.yck.wwp.interfaces.IDialogClickListener
import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData


open class CommonDialog(context: Context, itemsData: ItemsData, viewData: ViewData) : Dialog(context) {

    var mViewData = viewData
    var mItems = itemsData
    lateinit var mDialogListener:IDialogClickListener

    fun setDialogLayout(layouId: Int){
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(layouId)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        val this_window = window
        this_window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var params:WindowManager.LayoutParams = this_window!!.attributes
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        params.height = WindowManager.LayoutParams.MATCH_PARENT
        params.windowAnimations = R.style.AnimationPopupStyle
        this_window?.attributes = params
        this_window?.setGravity(Gravity.BOTTOM)
    }

    @SuppressLint("Range")
    fun setViewEvent(){
        //binding 을 사용하면 dialog 자체 layout ui 가 안먹음
        val constlDialogBody = findViewById<ConstraintLayout>(R.id.constl_dialog_body)
        val title = findViewById<TextView>(R.id.tv_title)
        val date = findViewById<TextView>(R.id.tv_date)
        val content = findViewById<TextView>(R.id.tv_content)
        val btnLeft = findViewById<TextView>(R.id.btn_left)
        val btnRight = findViewById<TextView>(R.id.btn_right)
        val iv_bookmark = findViewById<ImageView>(R.id.iv_bookmark)
        btnLeft.setOnClickListener {
            mDialogListener.leftBtn()
        }
        btnRight.setOnClickListener {
            mDialogListener.rightBtn()
        }
        if(mViewData.command.equals(COMMAND_DELETE)){//아이템 삭제 진행 여부 팝업
            title.text = context.resources.getString(R.string.str_dialog_content)
            date.text = mItems.date
            content.text = mItems.description
            btnLeft.text = context.resources.getString(R.string.str_dialog_btn_confirm)
            btnRight.text = context.resources.getString(R.string.str_dialog_btn_cancel)
            if(!mItems.color.isNullOrEmpty()){
                constlDialogBody.background = DIManager.viewCommonUtils.setDrawableSolidColor(mItems.color, R.drawable.dialog_layout_shape)
            }
            if(mItems.isfavorite.equals("true")){
                iv_bookmark.visibility = View.VISIBLE
            }else{
                iv_bookmark.visibility = View.GONE
            }
        }else if(mViewData.command.equals(COMMAND_BACKUP_COMPLETE)){//백업 완료 팝업
            title.text = context.resources.getString(R.string.str_dialog_db_backup_complete_title)
            var getFilePath = DIManager.repoSharedPref.getStringPref(PREF_SAVED_FILE_PATH_KEY)
            var string = String.format(context.resources.getString(R.string.str_dialog_db_backup_complete_content),getFilePath)
            content.text = Html.fromHtml(string)
            btnLeft.text = context.resources.getString(R.string.str_dialog_btn_confirm)
            btnRight.text = context.resources.getString(R.string.str_dialog_db_backup_btn_open_folder)
            iv_bookmark.visibility  = View.GONE
        }else if(mViewData.command.equals(COMMAND_BACKUP)){//백업 진행 여부 팝업
            title.text = context.resources.getString(R.string.str_dialog_db_backup_title)
            content.text = Html.fromHtml(context.resources.getString(R.string.str_dialog_db_backup_content))
            btnLeft.text = context.resources.getString(R.string.str_dialog_btn_ok)
            btnRight.text = context.resources.getString(R.string.str_dialog_btn_cancel)
            iv_bookmark.visibility  = View.GONE
        }else if(mViewData.command.equals(COMMAND_BACKUP_IMPORT)){//백업 불러오기 진행 여부 팝업
            title.text = context.resources.getString(R.string.str_dialog_db_backup_import_title)
            content.text = Html.fromHtml(context.resources.getString(R.string.str_dialog_db_backup_import_content))
            btnLeft.text = context.resources.getString(R.string.str_dialog_db_backup_import_btn_open_folder)
            btnRight.text = context.resources.getString(R.string.str_dialog_btn_cancel)
            iv_bookmark.visibility  = View.GONE
        }else if(mViewData.command.equals(COMMAND_REMOVE_DB)){//저장된 내용 제거하기
            title.text = context.resources.getString(R.string.str_dialog_db_remove_title)
            content.text = Html.fromHtml(context.resources.getString(R.string.str_dialog_db_remove_content))
            btnLeft.text = context.resources.getString(R.string.str_dialog_db_remove_btn_remove)
            btnRight.text = context.resources.getString(R.string.str_dialog_db_remove_btn_cancel)
            iv_bookmark.visibility  = View.GONE
        }else if(mViewData.command.equals(FIREBASE_CLOUDSTORE_COMPLETE)){//get Intro FirebaseData
            if(mViewData.resFirebaseCloud.title.isEmpty()) title.visibility = View.GONE
            if(mViewData.resFirebaseCloud.content.isEmpty()) content.visibility = View.GONE
            if(mViewData.resFirebaseCloud.btn_left.isEmpty()) btnLeft.visibility = View.GONE
            if(mViewData.resFirebaseCloud.btn_right.isEmpty()) btnRight.visibility = View.GONE
            iv_bookmark.visibility  = View.GONE
            title.text = mViewData.resFirebaseCloud.title
            content.text = Html.fromHtml(mViewData.resFirebaseCloud.content)
            btnLeft.text = mViewData.resFirebaseCloud.btn_left
            btnRight.text = mViewData.resFirebaseCloud.btn_right
        }
    }

    fun setOnDialogClickListener(listener: IDialogClickListener){
        mDialogListener = listener
    }


}//class end

