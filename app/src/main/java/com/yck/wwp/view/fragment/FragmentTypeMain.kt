package com.yck.wwp.view.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.yck.wwp.adapter.ActivityAdapter
import com.yck.wwp.interfaces.IOnItemClickListener
import com.yck.wwp.interfaces.IOnItemLongClickListener
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.databinding.FragMainBinding
import com.yck.wwp.di.DIManager
import com.yck.wwp.dialog.CommonDialog
import com.yck.wwp.interfaces.IDialogClickListener
import com.yck.wwp.interfaces.IFragmentTypeBase
import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData
import com.yck.wwp.view.activity.ActivityMain
import com.yck.wwp.view.activity.ActivityMainDetail
import com.yck.wwp.view.activity.ActivityPassWord
import com.yck.wwp.viewmodel.FragMainViewModel

class FragmentTypeMain:BaseFragment(),IFragmentTypeBase{

    lateinit var mContext: Context
    lateinit var mActivity: Activity
    lateinit var mAdapterActivity: ActivityAdapter
    lateinit var mFragMainBinding: FragMainBinding
    lateinit var mViewData:ViewData

    companion object{
        var mFragMainViewmodel = FragMainViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        mContext = requireContext()
        mActivity = requireActivity()
        mFragMainBinding = DataBindingUtil.inflate(inflater, R.layout.frag_main, container, false)
        registerObserver()
        return mFragMainBinding.root
    }
    //db at export to excel
    override fun onDestroy() {
        super.onDestroy()
        removeObserver()
    }

    override fun getItems(keyword: String) {
        mFragMainViewmodel.getDBItems(keyword)
    }

    override fun removeItems(viewData: ViewData, itemIdx: Int) {
        mFragMainViewmodel.removeDBItems(viewData, itemIdx)
    }

    override fun registerObserver() {
        mFragMainViewmodel?.mMainFragLiveData.observe(viewLifecycleOwner, Observer { it ->
            mViewData = it as ViewData
            callbackFromViewModel()
        })
    }

    override fun removeObserver() { mFragMainViewmodel?.mMainFragLiveData?.removeObservers(this) }

    override fun initAdapterView() {
        mAdapterActivity = mViewData.itemList?.let {
            ActivityAdapter(mContext, mViewData.itemList)
        }
        if(mFragMainBinding.rvMain!=null) mFragMainBinding.rvMain.removeAllViews()
        mFragMainBinding.rvMain.layoutManager = StaggeredGridLayoutManager(DIManager.repoSharedPref.getIntPref(
            PREF_SAVED_GRID_TYPE_KEY), LinearLayoutManager.VERTICAL)
        mFragMainBinding.rvMain.adapter = mAdapterActivity
        mFragMainBinding.rvMain.setHasFixedSize(false)
        mFragMainBinding.rvMain.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mContext,
            R.anim.recyclerview_layout_animation))
        mFragMainBinding.rvMain.getAdapter()?.notifyDataSetChanged()
        mFragMainBinding.rvMain.scheduleLayoutAnimation()
    }

    override fun initListener() {
        mAdapterActivity?.setOnItemClickListener(object : IOnItemClickListener {
            override fun onItemClick(itemsData: ItemsData, position: Int) {
                mFragMainViewmodel.getIdDBItems(COMMAND_CLICK_ITEM, itemsData.idx)
            }
        })
        mAdapterActivity?.setOnItemLongLcickListener(object : IOnItemLongClickListener {
            override fun onItemLongClick(itemsData: ItemsData, position: Int) {
                mViewData.command = COMMAND_DELETE
                showDialog(itemsData)
            }
        })
    }

    override fun callbackFromViewModel() {
        when(mViewData.command){
            COMMAND_CLICK_ITEM -> {//아이템 클릭
                var intent = Intent(mActivity, ActivityMainDetail::class.java)
                intent.putExtra(PUTEXTRA_KEY, mViewData)
                ActivityMain.comp_ActivityResultLauncher.launch(intent)//redirect ActivityMain Result
                startActivityAnimation()
                mViewData.command = ""
            }
            COMMAND_DELETE -> {
                getItems("")
                mViewData.command = ""
                mFragMainBinding.rvMain.getAdapter()?.notifyDataSetChanged()
                noItemImage()
            }
            else ->{
                initAdapterView()
                initListener()
                noItemImage()
            }
        }
    }

    override fun noItemImage() {
        if(mViewData.itemList.size==0){
            mFragMainBinding.constlNoItem.visibility = View.VISIBLE
            return
        }
        mFragMainBinding.constlNoItem.visibility = View.GONE
    }

    override fun showDialog(itemsData: ItemsData) {
        val dialog = CommonDialog(mContext, itemsData, mViewData)
        dialog.setDialogLayout(R.layout.dialog_layout)
        dialog.setViewEvent()
        dialog.setOnDialogClickListener(object : IDialogClickListener {
            override fun leftBtn() {
                ActivityMain.comp_ItemIDX = itemsData.idx

                if(mViewData.command.equals(COMMAND_DELETE)){

                    ActivityMain.compIsRefreshMainFragment = true
                    ActivityMain.compIsRefreshFavoriteFragment = true

                    if (DIManager.commonUtils.isLockNumSaved()) {
                        val intent = Intent(mContext, ActivityPassWord::class.java)
                        //아이템 제거
                        mViewData.pass_command = COMMAND_PASS_CONFIRM
                        intent.putExtra(PUTEXTRA_KEY, mViewData)
                        ActivityMain.comp_ActivityResultLauncher.launch(intent)
                        dialog.dismiss()
                        return
                    }
                    mFragMainViewmodel.removeDBItems(mViewData, itemsData.idx)
                    dialog.dismiss()
                }
            }

            override fun rightBtn() {
                dialog.dismiss()
            }
        })
        dialog.show()
    }


}//class end