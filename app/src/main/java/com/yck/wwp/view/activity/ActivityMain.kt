package com.yck.wwp.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import com.yck.wwp.R
import com.yck.wwp.adapter.FragAdapter
import com.yck.wwp.base.*
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ViewData
import com.yck.wwp.viewmodel.MainViewModel
import kotlinx.coroutines.GlobalScope
import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.exitProcess


class ActivityMain : BaseActivity(),View.OnClickListener {

    var mTabPosition = 0

    var mMainVieModel:MainViewModel = MainViewModel()
    lateinit var mTabTextList:ArrayList<String>
    lateinit var mTabIconList:ArrayList<Int>
    var mViewData = ViewData()

    companion object{
        var compIsRefreshMainFragment = true
        var compIsRefreshFavoriteFragment = true
        lateinit var comp_ActivityResultLauncher: ActivityResultLauncher<Intent>
        lateinit var mainBinding: com.yck.wwp.databinding.ActivityMainBinding
        var comp_ItemIDX = 0
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addActivity(this)
        setStatusBar(getColor(R.color.color_main_top))
        compIsRefreshMainFragment = true
        compIsRefreshFavoriteFragment = true
        mainBinding = DataBindingUtil.setContentView(this@ActivityMain, R.layout.activity_main)
        comp_ActivityResultLauncher = activityForResult()
        tabInit()
        setOnClickListener()
        mMainVieModel.getMainItem(mainBinding.edtSearch.text.toString())
    }


    override fun onDestroy() {
        super.onDestroy()
        finishApp()
    }

    @SuppressLint("ResourceAsColor")
    fun tabInit() {
        mTabTextList = arrayListOf(resources.getString(R.string.str_activity_main_tab_title_all),resources.getString(R.string.str_activity_main_tab_title_favorite))
        mTabIconList = arrayListOf(R.drawable.ic_tab_all_selected_45, R.drawable.ic_tab_favorite_book_selected_45)
        mainBinding.viewPager2.registerOnPageChangeCallback(object:
                ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                mTabPosition = position
            }

            override fun onPageScrollStateChanged(state: Int) {
                if(mTabPosition == TAB_MAIN_FRAG_PAGE && compIsRefreshMainFragment){
                    mMainVieModel.getMainItem(mainBinding.edtSearch.text.toString())
                    compIsRefreshMainFragment = false
                }else if(mTabPosition == TAB_FAVORITE_FRAG_PAGE && compIsRefreshFavoriteFragment){
                    mMainVieModel.getFavoriteItem(mainBinding.edtSearch.text.toString())
                    compIsRefreshFavoriteFragment = false
                }
            }

        })
        mainBinding.viewPager2.adapter = FragAdapter(this)
        TabLayoutMediator(tabLayout,mainBinding.viewPager2){ tab, position ->
            tab.setIcon(mTabIconList[position])
            tab.text = mTabTextList[position]
        }.attach()
    }

    override fun onBackPressed() {
        doubleClickFinishApp()
    }

    fun setOnClickListener() {
        mainBinding.ivbtnSetting.setOnClickListener(this)
        mainBinding.ivbtnAdd.setOnClickListener(this)
        mainBinding.btnClearText.setOnClickListener(this)
        mainBinding.ivbtnSearch.setOnClickListener(this)
        mainBinding.edtSearch.setOnKeyListener(object :View.OnKeyListener{
            override fun onKey(view: View?, i: Int, p2: KeyEvent?): Boolean {
                when (i){
                    KeyEvent.KEYCODE_ENTER->{
                        if(mTabPosition == TAB_MAIN_FRAG_PAGE){
                            mMainVieModel.getMainItem(mainBinding.edtSearch.text.toString())
                        }else if(mTabPosition == TAB_FAVORITE_FRAG_PAGE){
                            mMainVieModel.getFavoriteItem(mainBinding.edtSearch.text.toString())
                        }
                        return true
                    }
                    KeyEvent.KEYCODE_DEL->{
                        return false
                    }
                    KeyEvent.KEYCODE_BACK->{
                        finish()
                        return true
                    }
                }
                return true
            }
        })

        mainBinding.edtSearch.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(mainBinding.edtSearch.text.isNotEmpty()){
                    mainBinding.btnClearText.visibility = View.VISIBLE
                }else{
                    mainBinding.btnClearText.visibility = View.GONE
                }
            }
        })
    }

    fun activityForResult(): ActivityResultLauncher<Intent> {
        return registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if(result.resultCode== RESULT_CANCELED) return@registerForActivityResult

            if(result?.data?.getSerializableExtra(PUTEXTRA_KEY)!=null){
                var resultData = result?.data?.getSerializableExtra(PUTEXTRA_KEY)
                mViewData = resultData as ViewData
            }

            if(mViewData.command.equals(COMMAND_DELETE)){
                if(mTabPosition == TAB_MAIN_FRAG_PAGE){
                    mMainVieModel.removeMainItem(mViewData, comp_ItemIDX)
                }else if(mTabPosition == TAB_FAVORITE_FRAG_PAGE){
                    mMainVieModel.removeFavoriteItem(mViewData, comp_ItemIDX)
                }
            }

            compIsRefreshMainFragment = true
            compIsRefreshFavoriteFragment = true

            if(mTabPosition == TAB_MAIN_FRAG_PAGE){
                mMainVieModel.getMainItem(mainBinding.edtSearch.text.toString())
                compIsRefreshMainFragment = false
            }else{
                mMainVieModel.getFavoriteItem(mainBinding.edtSearch.text.toString())
                compIsRefreshFavoriteFragment = false
            }

        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.ivbtn_add ->{
                var intent = Intent(this@ActivityMain, ActivityMainDetail::class.java)
                intent.putExtra(PUTEXTRA_KEY, COMMAND_ADD)
                comp_ActivityResultLauncher.launch(intent)
                startActivityAnimation()
            }
            R.id.ivbtn_setting ->{
                var intent = Intent(this@ActivityMain, ActivitySetting::class.java)
                if(DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY).isNullOrEmpty()){
                    intent.putExtra(PUTEXTRA_KEY, COMMAND_PASS_REGISTER)
                }
                intent.putExtra(PUTEXTRA_KEY, COMMAND_PASS_CONFIRM)
                comp_ActivityResultLauncher.launch(intent)
                startActivityAnimation()
            }
            R.id.btn_clear_text ->{
                mainBinding.edtSearch.setText("")
            }
            R.id.ivbtn_search ->{
                if(mTabPosition == TAB_MAIN_FRAG_PAGE){
                    mMainVieModel.getMainItem(mainBinding.edtSearch.text.toString())
                }else if(mTabPosition == TAB_FAVORITE_FRAG_PAGE){
                    mMainVieModel.getFavoriteItem(mainBinding.edtSearch.text.toString())
                }
            }
            else ->{
                //None
            }
        }
    }


}//class end