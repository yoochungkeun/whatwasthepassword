package com.yck.wwp.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.databinding.ActivityMainDetailBinding
import com.yck.wwp.di.DIManager
import com.yck.wwp.global.GlobalData
import com.yck.wwp.model.ViewData
import com.yck.wwp.viewmodel.MainDetailViewModel
import kotlinx.android.synthetic.main.activity_main_detail.*
import timber.log.Timber


class ActivityMainDetail : BaseActivity() {


    lateinit var mGetResultData: ActivityResultLauncher<Intent>
    lateinit var mDetailBinding:ActivityMainDetailBinding
    var mViewData = ViewData()
    var mMaindDetailViewModel = MainDetailViewModel()
    var mColorBtnIsSeleceted = true
    var mFavoriteBtnIsSeleceted = false
    var mClickedID = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addActivity(this)
        setStatusBar(getColor(R.color.color_main_top))
        mDetailBinding = DataBindingUtil.setContentView(this@ActivityMainDetail, R.layout.activity_main_detail)
        mGetResultData = activityForResult()
        mMaindDetailViewModel.getIntentData(intent)
        initRegisterObserver()
        initsetOnListener()
        mDetailBinding.chkIdShowHide.isChecked = true
        mDetailBinding.chkPassShowHide.isChecked = true
    }

    override fun onBasePause() {
        super.onBasePause()
        mDetailBinding.chkIdShowHide.isChecked = true
        mDetailBinding.chkPassShowHide.isChecked = true
        hideIDContent()
        hideIDclipBoardImgBtnView()
        hidePassContent()
        hidePassClipBoardImgBtnView()
    }


    fun settingViewShowHide(){
        mDetailBinding.btnAdd.visibility = View.GONE//추가하기 버튼
        mDetailBinding.btnModify.visibility = View.GONE//수정하기 버튼
        mDetailBinding.btnModifyComplete.visibility = View.GONE//수정완료 버튼
        mDetailBinding.chkIdShowHide.visibility = View.GONE//id 값 보여주기/숨기기
        mDetailBinding.chkPassShowHide.visibility = View.GONE//pass 값 보여주기/숨기기
        mDetailBinding.ivbtnIdCopyClipboard.visibility = View.GONE//클립보드복사 ID
        mDetailBinding.ivbtnPassCopyClipboard.visibility = View.GONE//클립보드복사 Pass
        mDetailBinding.chkBookmark.visibility = View.INVISIBLE//즐겨찾기 버튼
        mDetailBinding.vwDescrUnderLine.visibility = View.INVISIBLE//설명 EdtText underline
        mDetailBinding.vwIdUnderLine.visibility = View.INVISIBLE//id EdtText underline
        mDetailBinding.vwPwUnderLine.visibility = View.INVISIBLE//pass EdtText underline
        mDetailBinding.edtDescr.visibility = View.INVISIBLE//설명 EdtText input
        mDetailBinding.tvDescr.visibility = View.INVISIBLE//설명 EdtText input

        if(mViewData.command.equals(COMMAND_ADD)){
            mDetailBinding.edtDescr.visibility = View.VISIBLE
            mDetailBinding.chkBookmark.visibility = View.VISIBLE
            mDetailBinding.btnAdd.visibility = View.VISIBLE
            mDetailBinding.vwDescrUnderLine.visibility = View.VISIBLE
            mDetailBinding.vwIdUnderLine.visibility = View.VISIBLE
            mDetailBinding.vwPwUnderLine.visibility = View.VISIBLE
            return
        }

        if(mViewData.command.equals(COMMAND_MODIFY)){
            mDetailBinding.edtDescr.visibility = View.VISIBLE
            mDetailBinding.chkBookmark.visibility = View.VISIBLE
            mDetailBinding.btnModifyComplete.visibility = View.VISIBLE
            mDetailBinding.vwDescrUnderLine.visibility = View.VISIBLE
            mDetailBinding.vwIdUnderLine.visibility = View.VISIBLE
            mDetailBinding.vwPwUnderLine.visibility = View.VISIBLE
            return
        }

        if(mViewData.command.equals(COMMAND_MODIFY_COMPLETE)||mViewData.command.equals(COMMAND_ADD_COMPLETE)){
            if(mViewData.item.isfavorite == FAVORITE_TRUE){
                mDetailBinding.chkBookmark.visibility = View.VISIBLE
            }
            return
        }

        if(mViewData.command.equals(COMMAND_CLICK_ITEM)){
            if(mViewData.item.isfavorite== FAVORITE_TRUE){
                mDetailBinding.chkBookmark.visibility = View.VISIBLE
            }
            mDetailBinding.tvDescr.visibility = View.VISIBLE
            mDetailBinding.tvDescr.movementMethod = LinkMovementMethod.getInstance()
            mDetailBinding.btnModify.visibility = View.VISIBLE
            mDetailBinding.chkIdShowHide.visibility = View.VISIBLE
            mDetailBinding.chkPassShowHide.visibility = View.VISIBLE
            return
        }
    }

    fun settingTitle() {
        if (mViewData.command.equals(COMMAND_ADD)) {
            mDetailBinding.tvTitle.setText(resources.getString(R.string.str_activity_main_detail_add_item_title))
            return
        }
        if(mViewData.command.equals(COMMAND_MODIFY)){
            mDetailBinding.tvDate.setText(mViewData.item.date)
            return
        }
        if (mViewData.command.equals(COMMAND_CLICK_ITEM)) {
            mDetailBinding.tvTitle.setText(mViewData.item.date)
            return
        }
    }

    fun activityForResult(): ActivityResultLauncher<Intent> {
        return registerForActivityResult(
                ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if(result.resultCode == RESULT_OK){
                if(mClickedID==R.id.chk_id_show_hide){
                    if(mDetailBinding.chkIdShowHide.isChecked){
                        mDetailBinding.chkIdShowHide.isChecked = false
                        showIDContent()
                        showIDclipBoardImgBtnView()
                    }
                }else if(mClickedID==R.id.chk_pass_show_hide){
                    if(mDetailBinding.chkPassShowHide.isChecked){
                        mDetailBinding.chkPassShowHide.isChecked = false
                        showPassContent()
                        showPassClipBoardImgBtnView()
                    }
                }else{
                    mViewData.command = COMMAND_MODIFY
                    mDetailBinding.edtId.setText(mViewData.item.id)
                    mDetailBinding.edtPw.setText(mViewData.item.pw)
                    clickEnable()
                    settingViewShowHide()
                    focusEditText()
                }
            }else{//RESULT_CANCEL
                if(mClickedID==R.id.chk_id_show_hide){
                    mDetailBinding.chkIdShowHide.isChecked = true
                    hideIDContent()
                    hideIDclipBoardImgBtnView()
                }else if(mClickedID==R.id.chk_pass_show_hide){
                    mDetailBinding.chkPassShowHide.isChecked = true
                    hidePassContent()
                    hidePassClipBoardImgBtnView()
                }
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        GlobalData.comp_activityList.remove(this)
        mMaindDetailViewModel?.mMainDetailLiveData?.removeObservers(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishProcessCancel()
    }

    fun initRegisterObserver(){
        mMaindDetailViewModel?.mMainDetailLiveData?.observe(this, Observer {
            var getViewData = it as ViewData
            mViewData = getViewData
            mViewData.item.id = DIManager.convertUtils.convertDeCryptString(mViewData.item.id)
            mViewData.item.pw = DIManager.convertUtils.convertDeCryptString(mViewData.item.pw)
            callbackFromViewModel()
        })
    }

    fun initsetOnListener() {
        mDetailBinding.constlBody.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                hideKeboard()
            }
        })
        mDetailBinding.ivbtnBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                finishProcessCancel()
            }
        })
        mDetailBinding.btnColor.setOnTouchListener(setOnTouchListener())
        mDetailBinding.chkBookmark.setOnTouchListener(setOnTouchListener())
        mDetailBinding.btnModifyComplete.setOnTouchListener(setOnTouchListener())
        mDetailBinding.btnModify.setOnTouchListener(setOnTouchListener())
        mDetailBinding.btnAdd.setOnTouchListener(setOnTouchListener())
        mDetailBinding.chkIdShowHide.setOnTouchListener(setOnTouchListener())
        mDetailBinding.chkPassShowHide.setOnTouchListener(setOnTouchListener())
        mDetailBinding.ivbtnIdCopyClipboard.setOnTouchListener(setOnTouchListener())
        mDetailBinding.ivbtnPassCopyClipboard.setOnTouchListener(setOnTouchListener())
    }

    fun setOnTouchListener():View.OnTouchListener{
        return object :View.OnTouchListener{
            @SuppressLint("ResourceType")
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                //id,pass 는 item 추가시 작동 막기
                if((mViewData.command.equals(COMMAND_ADD))&&((v?.id==R.id.iv_id)||(v?.id==R.id.iv_pass))) return true
                return when (event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        mClickedID = v?.id!!
                        when (v?.id) {
                            //ok
                            R.id.btn_color -> {
                                if (v is Button) {
                                    v.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.click_scale_anim_target_to_small))
                                }
                                false
                            }
                            //ok
                            R.id.chkBookmark -> {
                                if (v is CheckBox) {
                                    v.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.click_scale_anim_target_to_small))
                                }
                                false
                            }
                            else -> false
                        }
                    }
                    MotionEvent.ACTION_UP -> {
                        when (v?.id) {
                            R.id.btn_color -> {//ok
                                if (mColorBtnIsSeleceted) {
                                    showColorChoiceLayout()
                                    return false
                                }
                                hideColorChoiceLayout()
                                false
                            }
                            R.id.chkBookmark -> {//ok
                                if (mDetailBinding.chkBookmark.isChecked) {
                                    unCheckBookMarkImageView()
                                    return false
                                }
                                checkBookMarkImageView()
                                false
                            }
                            R.id.chk_id_show_hide -> {//아이디 show/hide
                                if (DIManager.commonUtils.isLockNumSaved() && mDetailBinding.chkIdShowHide.isChecked) {
                                    openActivityPassword(COMMAND_PASS_CONFIRM)
                                    return false
                                }
                                if (mDetailBinding.chkIdShowHide.isChecked) {
                                    showIDContent()
                                    showIDclipBoardImgBtnView()
                                } else {
                                    hideIDContent()
                                    hideIDclipBoardImgBtnView()
                                }
                                false
                            }
                            R.id.chk_pass_show_hide -> {
                                if (DIManager.commonUtils.isLockNumSaved() && mDetailBinding.chkPassShowHide.isChecked) {
                                    openActivityPassword(COMMAND_PASS_CONFIRM)
                                    return false
                                }
                                if (mDetailBinding.chkPassShowHide.isChecked) {
                                    showPassContent()
                                    showPassClipBoardImgBtnView()
                                } else {
                                    hidePassContent()
                                    hidePassClipBoardImgBtnView()
                                }
                                false
                            }
                            R.id.btn_modify -> {
                                mViewData.command = COMMAND_MODIFY
                                if (mViewData.item.color.isEmpty()) mViewData.item.color = resources.getString(R.color.color_pick_pastel_gray)
                                if (DIManager.commonUtils.isLockNumSaved()) {
                                    openActivityPassword(COMMAND_PASS_CONFIRM)
                                    return false
                                }
                                settingViewShowHide()
                                showIDContent()
                                showIDclipBoardImgBtnView()
                                showPassContent()
                                showPassClipBoardImgBtnView()
                                clickEnable()
                                focusEditText()
                                false
                            }
                            R.id.btn_modify_complete -> {
                                setDataForSave()
                                mViewData.command = COMMAND_MODIFY_COMPLETE
                                mMaindDetailViewModel.updateDBItems(mViewData)
                                false
                            }
                            R.id.btn_add -> {
                                setDataForSave()
                                mViewData.command = COMMAND_ADD_COMPLETE
                                mMaindDetailViewModel.addDBItems(mViewData)
                                false
                            }
                            R.id.ivbtn_id_copy_clipboard -> {
                                DIManager.commonUtils.copyTheclipBoard(mDetailBinding.edtId.text.toString())
                                Toast.makeText(this@ActivityMainDetail,resources.getString(R.string.str_toast_copy_the_clipboard),Toast.LENGTH_SHORT).show()
                                false
                            }
                            R.id.ivbtn_pass_copy_clipboard -> {
                                DIManager.commonUtils.copyTheclipBoard(mDetailBinding.edtPw.text.toString())
                                Toast.makeText(this@ActivityMainDetail,resources.getString(R.string.str_toast_copy_the_clipboard),Toast.LENGTH_SHORT).show()
                                false
                            }
                            else -> false
                        }
                    }
                    else -> false
                }
            }
        }
    }

    @SuppressLint("ResourceType")
    fun getSelectedColorString():String{
        if (mDetailBinding.radioBtnPastelRed.isChecked) return resources.getString(R.color.color_pick_pastel_red)
        else if (mDetailBinding.radioBtnPastelOrange.isChecked) return resources.getString(R.color.color_pick_pastel_orange)
        else if (mDetailBinding.radioBtnPastelYellow.isChecked) return resources.getString(R.color.color_pick_pastel_yellow)
        else if (mDetailBinding.radioBtnPastelGreen.isChecked) return resources.getString(R.color.color_pick_pastel_green)
        else if (mDetailBinding.radioBtnPastelBlue.isChecked) return resources.getString(R.color.color_pick_pastel_blue)
        else if (mDetailBinding.radioBtnPastelGray.isChecked) return resources.getString(R.color.color_pick_pastel_gray)
        else {
            if((mViewData.item.color.isEmpty())){
                return resources.getString(R.color.color_pick_pastel_gray)
            }//Click item dafault color
            return mViewData.item.color
        }
    }

    fun initsetData(){
        if(mViewData.item.color.isNotEmpty()){
            mDetailBinding.btnColor.background = DIManager.viewCommonUtils.setDrawableSolidColor(mViewData.item.color, R.drawable.oval_layout_shape)
        }
        mDetailBinding.tvDate.setText(mViewData.item.date)
        mDetailBinding.edtDescr.setText(mViewData.item.description)
        mDetailBinding.tvDescr.setText(mViewData.item.description)
    }

    //입력된 데이터 저장 전 셋팅
    fun setDataForSave(){
        mViewData.item.color = getSelectedColorString()
        mViewData.item.date = DIManager.commonUtils.getDate()
        mViewData.item.description = mDetailBinding.edtDescr.text.toString()
        mViewData.item.id = mDetailBinding.edtId.text.toString()
        mViewData.item.pw = mDetailBinding.edtPw.text.toString()
    }

    //비밀번호 페이지 이동
    fun openActivityPassword(command: String){
        val intent = Intent(this@ActivityMainDetail, ActivityPassWord::class.java)
        mViewData.pass_command = command
        intent.putExtra(PUTEXTRA_KEY, mViewData)
        mGetResultData.launch(intent)
    }

    //입력창 포커싱
    fun focusEditText(){
        mDetailBinding.edtDescr.isFocusableInTouchMode = true
        mDetailBinding.edtDescr.requestFocus()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(mDetailBinding.edtDescr, 0)
    }

    //즐겨찾기 이미지 초기화
    fun setFavoriteValue(){
        mDetailBinding.chkBookmark.isChecked = false
        mDetailBinding.chkBookmark
        if(mViewData.item.isfavorite.equals(FAVORITE_TRUE)){
            mDetailBinding.chkBookmark.isChecked = true
        }
    }

    //check book mark
    fun checkBookMarkImageView(){
        mViewData.item.isfavorite = FAVORITE_TRUE
        mDetailBinding.chkBookmark.isChecked = false
        mFavoriteBtnIsSeleceted = false
    }

    //un check book mark
    fun unCheckBookMarkImageView(){
        mViewData.item.isfavorite = FAVORITE_FALSE
        mDetailBinding.chkBookmark.isChecked = true
        mFavoriteBtnIsSeleceted = true
    }

    //show color choice layout
    fun showColorChoiceLayout(){
        mDetailBinding.rdobtnBody.visibility = View.VISIBLE
        mColorBtnIsSeleceted = false
    }

    //hide color choice layout
    fun hideColorChoiceLayout(){
        mDetailBinding.rdobtnBody.visibility = View.GONE
        mColorBtnIsSeleceted = true
    }

    fun showIDContent(){
        mDetailBinding.edtId.setText(mViewData.item.id)
    }

    fun showPassContent(){
        mDetailBinding.edtPw.setText(mViewData.item.pw)
    }

    fun hideIDContent(){
        mDetailBinding.edtId.setText(DIManager.convertUtils.convertStringToSecretString(mViewData.item.id))
    }

    fun hidePassContent(){
        mDetailBinding.edtPw.setText(DIManager.convertUtils.convertStringToSecretString(mViewData.item.pw))
    }

    fun showIDclipBoardImgBtnView(){
        mDetailBinding.ivbtnIdCopyClipboard.visibility = View.VISIBLE
    }

    fun hideIDclipBoardImgBtnView(){
        mDetailBinding.ivbtnIdCopyClipboard.visibility = View.GONE
    }

    fun showPassClipBoardImgBtnView(){
        mDetailBinding.ivbtnPassCopyClipboard.visibility = View.VISIBLE
    }

    fun hidePassClipBoardImgBtnView(){
        mDetailBinding.ivbtnPassCopyClipboard.visibility = View.GONE
    }

    fun callbackFromViewModel(){
        setFavoriteValue()
        settingViewShowHide()
        settingTitle()
        if(mViewData.command.equals(COMMAND_CLICK_ITEM)){
            clickDisable()
            initsetData()
            hideIDContent()
            hideIDclipBoardImgBtnView()
            hidePassContent()
            hidePassClipBoardImgBtnView()
            return
        }
        if(mViewData.command.equals(COMMAND_MODIFY_COMPLETE)||mViewData.command.equals(COMMAND_ADD_COMPLETE)){
            setResult(RESULT_OK)
            finish()
            finishActivityAnimation()
            return
        }
    }

    //키보드 내리기
    fun hideKeboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mDetailBinding.edtDescr.windowToken, 0)
        imm.hideSoftInputFromWindow(mDetailBinding.edtId.windowToken, 0)
        imm.hideSoftInputFromWindow(mDetailBinding.edtPw.windowToken, 0)
    }

    //버튼 활성화
    fun clickEnable(){
        mDetailBinding.btnColor.isEnabled = true
        mDetailBinding.chkBookmark.isEnabled = true
        mDetailBinding.edtId.isEnabled = true
        mDetailBinding.edtPw.isEnabled = true
    }

    //버튼 비활성화
    fun clickDisable(){
        mDetailBinding.btnColor.isEnabled = false
        mDetailBinding.chkBookmark.isEnabled = false
        mDetailBinding.edtId.isEnabled = false
        mDetailBinding.edtPw.isEnabled = false
    }

}//class end