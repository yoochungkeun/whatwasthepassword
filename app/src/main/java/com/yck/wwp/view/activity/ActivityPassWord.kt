package com.yck.wwp.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher

import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.databinding.ActivityPassWordBinding
import com.yck.wwp.global.GlobalData
import com.yck.wwp.model.ViewData
import com.yck.wwp.viewmodel.PassWordViewModel

class ActivityPassWord : BaseActivity() {

    lateinit var mPasswordBinding:com.yck.wwp.databinding.ActivityPassWordBinding
    var mViewData = ViewData()
    var mPasswordViewModel = PassWordViewModel()
    val PASS_MAXLength = 6
    lateinit var mImm:InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setStatusBar(resources.getColor(R.color.white))
        mPasswordBinding = DataBindingUtil.setContentView(this@ActivityPassWord,R.layout.activity_pass_word)
        mImm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        getIntentData(intent)
        initRegisterObserver()
        setOnClickListener()
        focusEditText()
        setTitleText()
        setContentText()
    }

    override fun onDestroy() {
        super.onDestroy()
        GlobalData.comp_activityList.remove(this)
        mPasswordViewModel?.mPasswordLiveData?.removeObservers(this)
        unfocusEditText()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(RESULT_CANCELED)
        finish()
    }

    fun initRegisterObserver(){
        mPasswordViewModel?.mPasswordLiveData?.observe(this, Observer {
            mViewData = it as ViewData
            callbackFromViewModel()
        })
    }

    fun setOnClickListener() {
        val animation_small: Animation = AnimationUtils.loadAnimation(applicationContext, R.anim.click_scale_anim_target_to_small)

        mPasswordBinding.ivbFinish.setOnClickListener {
            mPasswordBinding.ivbFinish.startAnimation(animation_small)
            setResult(RESULT_CANCELED)
            finish()
        }

        mPasswordBinding.edtPass.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s?.length==PASS_MAXLength){
                    mViewData.inputPass = s?.toString()
                    mPasswordViewModel.passProcess(mViewData)
                }
            }
            override fun afterTextChanged(s: Editable?) {

            }
        })
    }

    //LiveData Callback method
    fun callbackFromViewModel(){
        if(mViewData.pass_command.contains(SUCCESS)){//비번 로직 성공(확인,등록,해제)
            resultOfSuccessProcess()
        }else {//비번 로직 실패(확인,등록,해제)
            setTitleText()
            setContentText()
            resultOfFailedProcess()
        }
    }

    fun resultOfSuccessProcess(){
        if(mViewData.pass_command.equals(COMMAND_PASS_CONFIRM_SUCCESS)){
            var intent = Intent()
            mViewData.command = COMMAND_DELETE
            intent.putExtra(PUTEXTRA_KEY,mViewData)
            setResult(RESULT_OK,intent)
            finish()
            return
        }
        if(mViewData.pass_command.equals(COMMAND_PASS_UNLOCK_SUCCESS)){
            setResult(RESULT_OK)
            finish()
            return
        }
        if(mViewData.pass_command.equals(COMMAND_PASS_CHANGE_SUCCESS)){
            mViewData.pass_command = COMMAND_PASS_REGISTER
            mPasswordBinding.edtPass.setText("")
            setTitleText()
            setContentText()
            return
        }
        if(mViewData.pass_command.equals(COMMAND_PASS_REGISTER_SUCCESS)){
            mViewData.pass_command = COMMAND_PASS_REGISTER_RE
            mPasswordBinding.edtPass.setText("")
            setTitleText()
            setContentText()
            return
        }
        if(mViewData.pass_command.equals(COMMAND_PASS_REGISTER_RE_SUCCESS)){
            setResult(RESULT_OK)
            finish()
            return
        }
    }


    fun resultOfFailedProcess(){
        //비번 변경(비번확인 실패)실패
        if(mViewData.pass_command.equals(COMMAND_PASS_CHANGE_FAILED)){
            mViewData.pass_command = COMMAND_PASS_CHANGE
            mPasswordBinding.edtPass.setText("")
            return
        }
        //비번 등록 실패
        if(mViewData.pass_command.equals(COMMAND_PASS_REGISTER_RE_FAILED)){
            mViewData.pass_command = COMMAND_PASS_REGISTER
            mPasswordBinding.edtPass.setText("")
            return
        }
        //비번 해제(비번확인 실패)실패
        if(mViewData.pass_command.equals(COMMAND_PASS_UNLOCK_FAILED)){
            mViewData.pass_command = COMMAND_PASS_UNLOCK
            mPasswordBinding.edtPass.setText("")
            return
        }
        //비번 확인 실패
        if(mViewData.pass_command.equals(COMMAND_PASS_CONFIRM_FAILED)){
            mViewData.pass_command = COMMAND_PASS_CONFIRM
            mPasswordBinding.edtPass.setText("")
            return
        }
    }

    // 비번 페이지 제목 셋팅
    fun setTitleText(){
        when(mViewData.pass_command){
            COMMAND_PASS_CONFIRM ->{mPasswordBinding.tvTitle.setText(resources.getString(R.string.str_activity_password_title_confirm))}
            COMMAND_PASS_REGISTER ->{mPasswordBinding.tvTitle.setText(resources.getString(R.string.str_activity_password_title_register))}
            COMMAND_PASS_CHANGE ->{mPasswordBinding.tvTitle.setText(resources.getString(R.string.str_activity_password_title_change))}
            COMMAND_PASS_UNLOCK ->{mPasswordBinding.tvTitle.setText(resources.getString(R.string.str_activity_password_title_unlock))}
            COMMAND_PASS_REGISTER_RE,COMMAND_PASS_REGISTER_RE_FAILED ->{mPasswordBinding.tvTitle.setText(resources.getString(R.string.str_activity_password_title_register))}
        }
    }

    // 비번 페이지 내용 셋팅
    fun setContentText() {
        when(mViewData.pass_command){
            COMMAND_PASS_CONFIRM,COMMAND_PASS_REGISTER,COMMAND_PASS_UNLOCK,COMMAND_PASS_CHANGE->{
                mPasswordBinding.tvContent.setText(resources.getString(R.string.str_activity_password_content_confirm))
            }
            COMMAND_PASS_REGISTER_RE ->{
                mPasswordBinding.tvContent.setText(resources.getString(R.string.str_activity_password_content_register_re))
            }
            COMMAND_PASS_REGISTER_RE_FAILED, COMMAND_PASS_CHANGE_FAILED, COMMAND_PASS_UNLOCK_FAILED, COMMAND_PASS_CONFIRM_FAILED ->{
                mPasswordBinding.tvContent.setText(resources.getString(R.string.str_activity_password_content_not_match))
            }
        }
    }

    //입력창 포커싱
    fun focusEditText(){
        mPasswordBinding.edtPass.isFocusableInTouchMode = true
        mPasswordBinding.edtPass.requestFocus()
        mImm?.showSoftInput(mPasswordBinding.edtPass, InputMethodManager.SHOW_IMPLICIT);
    }

    //입력창 포커싱 해제
    fun unfocusEditText(){
        mPasswordBinding.edtPass.isFocusableInTouchMode = false
        mImm?.hideSoftInputFromWindow(getCurrentFocus()?.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //getPutExtra String Value
    fun getIntentData(intent: Intent){
        if(intent.getSerializableExtra(PUTEXTRA_KEY) is ViewData){
            mViewData = intent.getSerializableExtra(PUTEXTRA_KEY) as ViewData
        }else{
            mViewData.pass_command = intent.getStringExtra(PUTEXTRA_KEY).toString()
        }
    }

}//class end