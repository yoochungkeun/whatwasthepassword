package com.yck.wwp.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.databinding.ActivityIntroBinding
import com.yck.wwp.dialog.CommonDialog
import com.yck.wwp.interfaces.IDialogClickListener
import com.yck.wwp.interfaces.IIntroBase
import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData
import com.yck.wwp.viewmodel.IntroViewModel
import timber.log.Timber

class ActivityIntro :BaseActivity() {

    lateinit var mIntroActivityBinding: ActivityIntroBinding
    lateinit var mIntroAnim: Animation
    var mIntroViewModel = IntroViewModel(this)
    var mViewData = ViewData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mIntroActivityBinding = DataBindingUtil.setContentView(this@ActivityIntro, R.layout.activity_intro)
        registerObserver()
        initAnimUtil()
        initListener()
        mIntroViewModel.getFirebaseCloudStoreData()
    }

    fun registerObserver(){
        mIntroViewModel?.mIntroLiveData?.observe(this, Observer {
            mViewData = it as ViewData
            callbackFromViewModel()
        })
    }

    fun removeObserver(){
        mIntroViewModel?.mIntroLiveData?.removeObservers(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        removeObserver()
    }

    fun initAnimUtil(){
        mIntroAnim = AnimationUtils.loadAnimation(this, R.anim.scale_anim_target_to_intro_image)
    }

    fun initListener(){
        mIntroAnim.setAnimationListener(object : Animation.AnimationListener{
            override fun onAnimationStart(animation: Animation?) {
            }
            override fun onAnimationEnd(animation: Animation?) {
                startIntroAnimationView()
                mIntroViewModel.openActivitiyMain(ActivityMain())
            }
            override fun onAnimationRepeat(animation: Animation?) {
            }
        })
    }

    fun startIntroAnimationView() {
        mIntroActivityBinding.ivIntroImage.setImageDrawable(resources.getDrawable(R.drawable.bizi_app_icon_img))
        mIntroActivityBinding.tvIntroText.setTextColor(resources.getColor(R.color.color_intro_text))
    }

    fun callbackFromViewModel() {
        showDialog()
    }

    fun showDialog() {
        var item = ItemsData()
        val dialog = CommonDialog(this, item, mViewData)
        dialog.setDialogLayout(R.layout.dialog_layout)
        dialog.setViewEvent()
        dialog.setOnDialogClickListener(object : IDialogClickListener {
            override fun leftBtn() {
                dialog.dismiss()
                mIntroActivityBinding.ivIntroImage.startAnimation(mIntroAnim)
            }
            override fun rightBtn() {
                dialog.dismiss()
            }
        })
        dialog.show()
    }

}//class end