package com.yck.wwp.view.activity

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.di.DIManager
import com.yck.wwp.dialog.CommonDialog
import com.yck.wwp.global.GlobalData
import com.yck.wwp.interfaces.IDialogClickListener
import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData
import com.yck.wwp.utils.DBimportExportUitls
import com.yck.wwp.viewmodel.SettingViewModel
import kotlinx.android.synthetic.main.activity_main_detail.*
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.activity_setting.view.*
import kotlinx.coroutines.GlobalScope
import timber.log.Timber
import java.io.File


class ActivitySetting : BaseActivity(), View.OnClickListener {

    lateinit var mGetResultData: ActivityResultLauncher<Intent>
    lateinit var mSettingBinding: com.yck.wwp.databinding.ActivitySettingBinding
    var mSettingViewModel = SettingViewModel()
    var mViewData = ViewData()
    var mIsAllGranted = true
    var mSwitchOnoff = false
    var mGridTypeNum = DIManager.repoSharedPref.getIntPref(PREF_SAVED_GRID_TYPE_KEY)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBar(getColor(R.color.color_main_top))
        addActivity(this)
        mSettingBinding = DataBindingUtil.setContentView(this@ActivitySetting, R.layout.activity_setting)
        registerObserver()

        GlobalScope.run {
            DIManager.dbUtils.initDB()
            mViewData.itemList = DIManager.dbUtils.getAllItems()
        }
        setSwitchImageView()
        mGetResultData = activityForResult()
        setOnClickListener()
        setTitle(resources.getString(R.string.str_activity_setting_title))
        initCurrentyListTypeImageView()
    }

    override fun onDestroy() {
        super.onDestroy()
        GlobalData.comp_activityList.remove(this)
        removeObserver()
    }

    fun registerObserver() {
        mSettingViewModel?.mSettingLiveData.observe(this, Observer { it ->
            mViewData = it as ViewData
            callBackFromViewModel()
        })
    }

    fun removeObserver() {
        mSettingViewModel?.mSettingLiveData?.removeObservers(this)
    }

    fun callBackFromViewModel(){
        if(mViewData.command.equals(COMMAND_BACKUP)){
            mViewData.command = COMMAND_BACKUP_COMPLETE
            mSettingViewModel.createDataFile(DBimportExportUitls(this), mViewData)
        }else if(mViewData.command.equals(COMMAND_BACKUP_COMPLETE)){
            hideLoading()
            Toast.makeText(this, resources.getString(R.string.str_toast_db_backup_complete_content), Toast.LENGTH_SHORT).show()
        }else if(mViewData.command.equals(COMMAND_REMOVE_DB_COMPLETE)){
            DIManager.dbUtils.initDB()
            DIManager.dbUtils.clearDBData()
            mViewData.command = COMMAND_BACKUP_IMPORT
            hideLoading()
            Toast.makeText(this, resources.getString(R.string.str_toast_db_remove_complete_content), Toast.LENGTH_SHORT).show()
            showFileChoiceDialog()

        }else if(mViewData.command.equals(COMMAND_BACKUP_IMPORT_COMPLETE)){
            hideLoading()
            finishApp()
            startActivity(Intent(this@ActivitySetting, ActivityIntro::class.java))
        }
    }


    fun makeDir() {
        val root = Environment.getExternalStorageDirectory().absolutePath //내장에 만든다
        val directoryName = "abc"
        val myDir = File("$root/$directoryName")
        if (!myDir.exists()) {
            val wasSuccessful = myDir.mkdirs()
            if (!wasSuccessful) {
                Timber.e("file: was not successful.")
            } else {
                Timber.e("file: 최초로 앨범파일만듬.$root/$directoryName")
            }
        } else {
            Timber.e("file: " + root + "/" + directoryName + "already exists")
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            0 -> {
                if (grantResults.isNotEmpty()) {
                    for (grant in grantResults) {
                        if (grant != PackageManager.PERMISSION_GRANTED) {
                            mIsAllGranted = false
                        }
                    }
                }
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                } else {

                }
            }
        }

        if(mIsAllGranted){//권한허가 상태
            mViewData.command = COMMAND_BACKUP
            showDialog()
        }

    }

    fun setTitle(title_text: String) {
        if ((title_text).isNotEmpty()) {
            mSettingBinding.tvTitle.setText(title_text)
        }
    }

    fun initCurrentyListTypeImageView() {
        if (DIManager.repoSharedPref.getIntPref(PREF_SAVED_GRID_TYPE_KEY) == RECYCLERVIEW_ITEM_GRID_TYPE_1) {
            mSettingBinding.ivCurrentyGridType.setImageResource(R.drawable.ic_grid_type_count_1_45)
            return
        }
        if (DIManager.repoSharedPref.getIntPref(PREF_SAVED_GRID_TYPE_KEY) == RECYCLERVIEW_ITEM_GRID_TYPE_2) {
            mSettingBinding.ivCurrentyGridType.setImageResource(R.drawable.ic_grid_type_count_2_45)
            return
        }
        if (DIManager.repoSharedPref.getIntPref(PREF_SAVED_GRID_TYPE_KEY) == RECYCLERVIEW_ITEM_GRID_TYPE_3) {
            mSettingBinding.ivCurrentyGridType.setImageResource(R.drawable.ic_grid_type_count_3_45)
            return
        }
    }

    override fun onBackPressed() {
        if(mSettingViewModel.isChangedGridType(mGridTypeNum)){
            setResult(RESULT_OK)
        }else{
            setResult(RESULT_CANCELED)
        }
        finish()
        finishActivityAnimation()
    }

    fun setOnClickListener() {
        mSettingBinding.ivbtnBack.setOnClickListener(this)
        mSettingBinding.switchLock.setOnClickListener(this)
        mSettingBinding.constlSetPassBody.setOnClickListener(this)
        mSettingBinding.constlSetPassBodyChangeNum.setOnClickListener(this)
        mSettingBinding.constlSettingBackupDbBody.setOnClickListener(this)
        mSettingBinding.constlSettingBackupImportDbBody.setOnClickListener(this)
        mSettingBinding.constlSettingRemoveDbBody.setOnClickListener(this)
        mSettingBinding.ivBtnGridTypeOne.setOnClickListener(setOnClickForGridTypeImageButton())
        mSettingBinding.ivBtnGridTypeTwo.setOnClickListener(setOnClickForGridTypeImageButton())
        mSettingBinding.ivBtnGridTypeThree.setOnClickListener(setOnClickForGridTypeImageButton())
    }

    fun activityForResult(): ActivityResultLauncher<Intent> {
        return registerForActivityResult(
                ActivityResultContracts.StartActivityForResult()
        ) { result ->
            setSwitchImageView()

            if(mViewData.command.equals(COMMAND_BACKUP)&&result.resultCode == RESULT_OK){
                if(DIManager.commonUtils.requestPermissions(this)){
                    mViewData.command = COMMAND_BACKUP
                    showDialog()
                }
            }else if(mViewData.command.equals(COMMAND_REMOVE_DB)&&result.resultCode == RESULT_OK){
                if(mViewData.itemList.size<=0){
                    mViewData.is_clear_db = true
                    mViewData.command = COMMAND_BACKUP_IMPORT
                }else{
                    mViewData.is_clear_db = false
                    mViewData.command = COMMAND_REMOVE_DB
                }
                showDialog()
            }else{

            }

        }
    }

    //비번on/off버튼 이미지 셋팅
    fun setSwitchImageView() {
        if (!(DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY)).isNullOrEmpty()) {
            mSettingBinding.switchLock.isChecked = true
            mSwitchOnoff = true
            return
        }
        mSettingBinding.switchLock.isChecked = false
        mSwitchOnoff = false
    }

    //버튼 리스너
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.switch_lock -> {
                if (switch_lock.isChecked) {
                    mSwitchOnoff = false
                    switchOnOffOpenActivityProcess()
                    return
                }
                mSwitchOnoff = true
                switchOnOffOpenActivityProcess()
            }
            R.id.constl_set_pass_body -> {
                if (switch_lock.isChecked) {
                    switch_lock.isChecked = false
                    switchOnOffOpenActivityProcess()
                    return
                }
                switch_lock.isChecked = true
                switchOnOffOpenActivityProcess()
            }
            R.id.constl_set_pass_body_change_num -> {
                //비번 변경
                if (DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY).isNullOrEmpty()) {
                    Toast.makeText(
                            this,
                            resources.getString(R.string.str_activity_setting_try_change_password),
                            Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                openPassActivity(COMMAND_PASS_CHANGE)
            }
            //백업하기
            R.id.constl_setting_backup_db_body -> {
                mViewData.command = COMMAND_BACKUP
                if (DIManager.commonUtils.isLockNumSaved()) {
                    openPassActivity(COMMAND_PASS_CONFIRM)
                    return
                }
                if (DIManager.commonUtils.requestPermissions(this)) {
                    showDialog()
                }
            }
            //백업 데이터 불러오기
            R.id.constl_setting_backup_import_db_body -> {
                mViewData.command = COMMAND_REMOVE_DB
                if (DIManager.commonUtils.isLockNumSaved()) {
                    openPassActivity(COMMAND_PASS_CONFIRM)
                    return
                }
                if (mViewData.itemList.size <= 0) {
                    mViewData.is_clear_db = true
                    mViewData.command = COMMAND_BACKUP_IMPORT
                } else {
                    mViewData.is_clear_db = false
                    mViewData.command = COMMAND_REMOVE_DB
                }
                showDialog()
            }
            R.id.ivbtn_back -> {
                //설정 페이지 종료
                if (mSettingViewModel.isChangedGridType(mGridTypeNum)) {
                    setResult(RESULT_OK)
                } else {
                    setResult(RESULT_CANCELED)
                }
                finish()
                finishActivityAnimation()
            }
        }
    }

    //파일선택 리스트 다이얼로그 리스너
    fun setDialogOnClickListener(arrayFiles: Array<String?>): DialogInterface.OnClickListener {
        return object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, arr_idx: Int) {
                showLoading()
                var path = "${DIManager.commonUtils.getExternalFilesDirUri()}/${arrayFiles.get(arr_idx)}"
                mSettingViewModel.importBackupFile(DBimportExportUitls(this@ActivitySetting), path, mViewData)
            }
        }
    }

    //정렬방식 버튼 리스너
    fun setOnClickForGridTypeImageButton(): View.OnClickListener {
        return object : View.OnClickListener {
            override fun onClick(v: View?) {
                when (v?.id) {
                    R.id.iv_btn_grid_type_one -> {
                        mGridTypeNum = RECYCLERVIEW_ITEM_GRID_TYPE_1
                    }
                    R.id.iv_btn_grid_type_two -> {
                        mGridTypeNum = RECYCLERVIEW_ITEM_GRID_TYPE_2
                    }
                    R.id.iv_btn_grid_type_three -> {
                        mGridTypeNum = RECYCLERVIEW_ITEM_GRID_TYPE_3
                    }
                }
                changeGridTypeView()
            }
        }
    }

    //정렬 이미지
    fun changeGridTypeView() {
        when (mGridTypeNum) {
            RECYCLERVIEW_ITEM_GRID_TYPE_1 -> {
                mSettingBinding.ivCurrentyGridType.setImageResource(R.drawable.ic_grid_type_count_1_45)
            }
            RECYCLERVIEW_ITEM_GRID_TYPE_2 -> {
                mSettingBinding.ivCurrentyGridType.setImageResource(R.drawable.ic_grid_type_count_2_45)
            }
            RECYCLERVIEW_ITEM_GRID_TYPE_3 -> {
                mSettingBinding.ivCurrentyGridType.setImageResource(R.drawable.ic_grid_type_count_3_45)
            }
        }
    }

    //비번 입력 페이지 오픈
    fun openPassActivity(command: String) {
        val intent = Intent(this@ActivitySetting, ActivityPassWord::class.java)
        intent.putExtra(PUTEXTRA_KEY, command)
        mGetResultData.launch(intent)
    }

    //Switch On/off process
    fun switchOnOffOpenActivityProcess() {
        if (mSwitchOnoff == false) {
            //비번 등록
            if (DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY).isNullOrEmpty()) {
                openPassActivity(COMMAND_PASS_REGISTER)
                return
            }
            //비번 확인
            openPassActivity(COMMAND_PASS_CONFIRM)
            return
        }
        //비번 해제
        openPassActivity(COMMAND_PASS_UNLOCK)
    }

    fun showDialog() {
        var item = ItemsData()
        val dialog = CommonDialog(this, item, mViewData)
        dialog.setDialogLayout(R.layout.dialog_layout)
        dialog.setViewEvent()
        dialog.setOnDialogClickListener(object : IDialogClickListener {
            override fun leftBtn() {
                dialog.dismiss()
                if (mViewData.command.equals(COMMAND_BACKUP)) {//DB 백업 파일 생성
                    showLoading()
                    mSettingViewModel.getAllDBItemsForCreateFile(mViewData)
                } else if (mViewData.command.equals(COMMAND_REMOVE_DB)) {//DB table 제거
                    mSettingViewModel.clearDB(mViewData)
                } else if (mViewData.command.equals(COMMAND_BACKUP_IMPORT)) {//DB 백업 파일 불러오기
                    showFileChoiceDialog()
                }
            }

            override fun rightBtn() {
                dialog.dismiss()
                if (mViewData.command.equals(COMMAND_BACKUP_COMPLETE)) {
                    DIManager.commonUtils.openTheExportBackUpFileFolder(this@ActivitySetting)
                }
                mViewData.command = ""
            }
        })
        dialog.show()
    }

    //저장된 데이터 파일 선택 다이얼로그
    fun showFileChoiceDialog(){
        var arrayFiles = DIManager.convertUtils.convertArrayListToArray(DIManager.commonUtils.getExternalDirFileList())
        var mBuilder = AlertDialog.Builder(this@ActivitySetting)
        mBuilder.setCancelable(false)
        if(DIManager.repoSharedPref.getStringPref(PREF_SAVED_FILE_PATH_KEY).isNullOrEmpty()){
            Toast.makeText(this@ActivitySetting, resources.getString(R.string.str_dialog_db_backup_not_exist_file_content), Toast.LENGTH_SHORT).show()
            return
        }
        var string = String.format(resources.getString(R.string.str_dialog_db_backup_file_path), DIManager.repoSharedPref.getStringPref(PREF_SAVED_FILE_PATH_KEY))
        mBuilder.setTitle(string)
        mBuilder.setItems(arrayFiles, setDialogOnClickListener(arrayFiles)).create().show()
        mViewData.command = COMMAND_BACKUP_IMPORT_COMPLETE
    }

    fun showLoading(){
        DIManager.commonUtils.showLoading(mSettingBinding.constlLoadingBody, this)
    }

    fun hideLoading(){
        DIManager.commonUtils.hideLoading(mSettingBinding.constlLoadingBody, this)
    }


}//class end