package com.yck.wwp.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yck.wwp.base.DB_RM_CONTACTS
import com.yck.wwp.interfaces.IItemsDAO
import com.yck.wwp.model.ItemsData


@Database(entities = [ItemsData::class],version = 1,exportSchema = false)
abstract class RepoRmDatabase : RoomDatabase(){

    //자료형 RmContactDao 의 추상함수 생성
    abstract fun itmes_dao(): IItemsDAO

    companion object{
        private var instance: RepoRmDatabase?=null
        @Synchronized
        fun getInstance(context: Context): RepoRmDatabase?{
            if(instance ==null){
                instance = Room.databaseBuilder(context.applicationContext,RepoRmDatabase::class.java,DB_RM_CONTACTS).allowMainThreadQueries().build()
            }
            return instance
        }
    }


}//class end