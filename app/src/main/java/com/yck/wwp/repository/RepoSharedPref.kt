package com.yck.wwp.repository

import android.content.Context
import android.content.SharedPreferences
import com.yck.wwp.base.PREF_BASE_PREFNAME

class RepoSharedPref(context: Context){

    private val mContext:Context = context

    open fun setStringPref(key:String,value:String){
        val pref = mContext.getSharedPreferences(PREF_BASE_PREFNAME,Context.MODE_PRIVATE)
        if(pref==null) return
        val editor:SharedPreferences.Editor = pref.edit()
        editor.putString(key, value)
        editor.commit()
    }

    open fun getStringPref(key:String):String{
        val pref = mContext.getSharedPreferences(PREF_BASE_PREFNAME,Context.MODE_PRIVATE)
        if(pref==null) return ""
        return pref.getString(key,"")!!
    }

    open fun setIntPref(key:String,value:Int){
        val pref = mContext.getSharedPreferences(PREF_BASE_PREFNAME,Context.MODE_PRIVATE)
        if(pref==null) return
        val editor:SharedPreferences.Editor = pref.edit()
        editor.putInt(key, value)
        editor.commit()
    }

    open fun getIntPref(key:String):Int{
        val pref = mContext.getSharedPreferences(PREF_BASE_PREFNAME,Context.MODE_PRIVATE)
        if(pref==null) return 3
        return pref.getInt(key,3)!!
    }

}//class end