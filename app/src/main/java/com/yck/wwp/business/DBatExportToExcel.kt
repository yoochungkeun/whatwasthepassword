package com.yck.wwp.business

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat.startActivity
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.di.DIManager
import com.yck.wwp.dialog.CommonDialog
import com.yck.wwp.interfaces.IDialogClickListener
import com.yck.wwp.model.Items
import com.yck.wwp.model.ViewData
import com.yck.wwp.view.activity.ActivityMain
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class DBatExportToExcel() {

    lateinit var mActivity: Activity
    val mWorkbook: Workbook = HSSFWorkbook()
    var mTitleList = emptyArray<String>()
    lateinit var mSheet: Sheet
    lateinit var mRow: Row

    constructor(activity: Activity): this(){
        mActivity = activity
    }

    fun startExcelWork(item_list:MutableList<Items>) {
        mTitleList = arrayOf(
            mActivity.resources.getString(R.string.str_business_excel_cell_A1),
            mActivity.resources.getString(R.string.str_business_excel_cell_B1),
            mActivity.resources.getString(R.string.str_business_excel_cell_C1),
            mActivity.resources.getString(R.string.str_business_excel_cell_D1),
            mActivity.resources.getString(R.string.str_business_excel_cell_E1)
        )
        //row : in Excel 1,2,3,4...
        //cell : in Excel A,B,C,D...
        mSheet = mWorkbook.createSheet()
        mRow = mSheet.createRow(0) // 새로운 행 생성
        var cell: Cell
        for (i in 0 until mTitleList.size) {
            cell = mRow.createCell(i)
            cell.setCellValue(mTitleList.get(i))
        }
        try {
            for (i in 0 until item_list.size) { // 데이터 엑셀에 입력
                var item = item_list[i]
                mRow = mSheet.createRow(i + 1)

                //number
                cell = mRow.createCell(0)
                cell.setCellValue("${i}")

                //description
                cell = mRow.createCell(1)
                cell.setCellValue(item.description)

                //id
                cell = mRow.createCell(2)
                cell.setCellValue(DIManager.convertUtils.convertDeCryptString(item.id))

                //pass
                cell = mRow.createCell(3)
                cell.setCellValue(DIManager.convertUtils.convertDeCryptString(item.pw))

                //isFavorite
                cell = mRow.createCell(4)

                cell.setCellValue(DIManager.convertUtils.convertIsFavoriteToOX(item))
            }
        } catch (e: Exception) {
            Timber.e("e.message : ${e.message}")
        }
    }

    fun createExcelFile() {
        val xlsFile = File(
            mActivity.getExternalFilesDir(null),
            "${BUISINESS_EXCEL_FILE_NAME}_${DIManager.commonUtils.getDate()}.xls"
        )
        try {
            val os = FileOutputStream(xlsFile)
            if (mWorkbook == null) return
            mWorkbook.write(os) // 외부 저장소에 엑셀 파일 생성
            //showDialog()
        } catch (e: IOException) {
            Timber.e("e.message : ${e.message}")
        }
    }


    fun openTheExportExcelFileFolder() {
        val selectedUri: Uri = Uri.parse(mActivity.getExternalFilesDir(null).toString())
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(selectedUri, "resource/folder")
        if (intent.resolveActivityInfo(mActivity.packageManager, 0) != null) {
            mActivity.startActivity(intent)
        } else {
        }
    }





}//class end