package com.yck.wwp.interfaces

interface IDialogClickListener {
    fun leftBtn()
    fun rightBtn()
}