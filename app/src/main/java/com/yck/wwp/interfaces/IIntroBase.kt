package com.yck.wwp.interfaces

import android.app.Activity
import com.yck.wwp.model.ViewData
import com.yck.wwp.viewmodel.IntroViewModel

interface IIntroBase {

    interface view{
        fun showIntroAnimationView()
    }

    interface viewModel{
        fun openActivitiyMain(activity: Activity)
        fun getFirebaseCloudStoreData()
    }

}//class end