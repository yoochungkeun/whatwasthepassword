package com.yck.wwp.interfaces

import com.yck.wwp.model.ItemsData

interface IOnItemClickListener{

    fun onItemClick(itemsData: ItemsData, position:Int)

}//class end
