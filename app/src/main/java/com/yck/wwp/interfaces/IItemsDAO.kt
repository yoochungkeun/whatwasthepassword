package com.yck.wwp.interfaces

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.yck.wwp.model.ItemsData


@Dao
interface IItemsDAO {

    //Add Item
    @Insert
    fun insertItem(vararg items: ItemsData)

    //DB 전체 값 가져오기
    @Query("SELECT * FROM AccountItems ORDER BY description DESC")
    fun getAll():MutableList<ItemsData>

    //DB 삭제하기
    @Query("DELETE FROM AccountItems WHERE idx = :id")
    fun deleteById(id: Int)

    //DB Like검색(검색어 입력 검색)
    @Query("SELECT * FROM AccountItems WHERE description LIKE '%' || :keyword || '%' ORDER BY date DESC")
    fun getSearch(keyword: String):MutableList<ItemsData>

    //DB Like검색(검색어 입력 검색 & 즐겨찾기 등록 포함)
    @Query("SELECT * FROM AccountItems WHERE isfavorite = :isfavorite and description LIKE '%' || :keyword || '%' ORDER BY date DESC")
    fun getSearchinFavorite(keyword: String,isfavorite: String):MutableList<ItemsData>

    //DB (즐겨찾기등록된것만 검색)
    @Query("SELECT * FROM AccountItems WHERE isfavorite = :isfavorite ORDER BY date DESC")
    fun getSearchFavorite(isfavorite: String):MutableList<ItemsData>

    //DB "id" 검색)
    @Query("SELECT * FROM AccountItems WHERE idx = :idx ORDER BY date DESC")
    fun getIdItem(idx: Int):ItemsData

    //DB update
    @Query("UPDATE AccountItems SET description = :description , id = :id ,pw = :pw , date = :date , isfavorite =:isfavorite , color = :color WHERE idx = :idx")
    fun updateIdItem(idx: Int,description:String,id:String,pw:String,date:String,isfavorite:String,color:String)

/*
    //Remove Table
    @Query("DROP TABLE AccountItems")
    fun removeTable()
*/

}//class end

