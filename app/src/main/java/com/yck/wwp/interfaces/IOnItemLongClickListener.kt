package com.yck.wwp.interfaces

import com.yck.wwp.model.ItemsData

interface IOnItemLongClickListener{

    fun onItemLongClick(itemsData: ItemsData, position:Int)

}//class end
