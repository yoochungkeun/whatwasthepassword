package com.yck.wwp.interfaces

import com.yck.wwp.model.ItemsData
import com.yck.wwp.model.ViewData

interface IFragmentTypeBase {

    fun getItems(keyword:String)
    fun removeItems(viewData: ViewData, itemIdx:Int)
    fun registerObserver()
    fun removeObserver()
    fun initAdapterView()
    fun initListener()
    fun callbackFromViewModel()
    fun noItemImage()
    fun showDialog(itemsData: ItemsData)

}