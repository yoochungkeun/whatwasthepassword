package com.yck.wwp.utils

import android.content.Context
import android.os.Build
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64.*
import timber.log.Timber
import java.math.BigInteger
import java.security.GeneralSecurityException
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.spec.RSAKeyGenParameterSpec
import java.security.spec.RSAKeyGenParameterSpec.F4
import java.util.*
import javax.crypto.Cipher
import javax.security.auth.x500.X500Principal


class AndroidKeyStoreUtil(context: Context){

    private val mContext = context
    private val KEY_LENGTH_BIT = 2048
    private val VALIDITY_YEARS = 25
    private val KEY_PROVIDER_NAME = "AndroidKeyStore"
    private val KEYSOTRE_INSTANCE_TYPE = "AndroidKeyStore"
    private val CIPHER_ALGORITHM = "${KeyProperties.KEY_ALGORITHM_RSA}/" + "${KeyProperties.BLOCK_MODE_ECB}/" + KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1
    private var _isSupported = false
    private lateinit var keyEntry: KeyStore.Entry

    private val isSupported: Boolean
        get() = _isSupported


    fun init(hashkey:String) {
        if (isSupported) {
            return
        }

        val alias = "${hashkey}.rsakeypairs"
        val keyStore = KeyStore.getInstance(KEYSOTRE_INSTANCE_TYPE).apply {
            load(null)
        }

        var result: Boolean = if (keyStore.containsAlias(alias)) {
            true
        } else {
            // 안드로이드 M(API 23) 이상인 경우 RSA 알고리즘 사용 가능
            if (initAndroidM(alias)) {
                true
            } else {
                initAndroidL(alias)
            }
        }

        this.keyEntry = keyStore.getEntry(alias, null)
        _isSupported = result
    }

    private fun initAndroidM(alias: String): Boolean {
        try {
            with(KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, KEY_PROVIDER_NAME), {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val spec = KeyGenParameterSpec.Builder(alias,
                        KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                        .setAlgorithmParameterSpec(RSAKeyGenParameterSpec(KEY_LENGTH_BIT, F4))
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                        .setDigests(KeyProperties.DIGEST_SHA512,
                            KeyProperties.DIGEST_SHA384,
                            KeyProperties.DIGEST_SHA256)
                        .setUserAuthenticationRequired(false)
                        .build()

                    initialize(spec)
                    generateKeyPair()
                } else {
                    return false
                }
            })
            return true
        } catch (e: GeneralSecurityException) {
            Timber.e("GeneralSecurityException : ${e.message}")
            return false
        }
    }

    private fun initAndroidL(alias: String): Boolean {
        try {
            with(KeyPairGenerator.getInstance("RSA", KEY_PROVIDER_NAME), {
                val start = Calendar.getInstance(Locale.ENGLISH)
                val end = Calendar.getInstance(Locale.ENGLISH).apply { add(Calendar.YEAR, VALIDITY_YEARS) }
                val spec = KeyPairGeneratorSpec.Builder(mContext)
                    .setKeySize(KEY_LENGTH_BIT)
                    .setAlias(alias)
                    .setSubject(X500Principal("CN=hybridApp, OU=Android debug"))
                    .setSerialNumber(BigInteger.ONE)
                    .setStartDate(start.time)
                    .setEndDate(end.time)
                    .build()

                initialize(spec)
                generateKeyPair()
            })

            return true
        } catch (e: GeneralSecurityException) {
            Timber.e("GeneralSecurityException : ${e.message}")
            return false
        }
    }

    fun encrypt(plainText: String): String {
        if (!_isSupported) {
            return plainText
        }
        val cipher = Cipher.getInstance(CIPHER_ALGORITHM).apply {
            init(Cipher.ENCRYPT_MODE, (keyEntry as KeyStore.PrivateKeyEntry).certificate.publicKey)
        }
        val bytes = plainText.toByteArray(Charsets.UTF_8)
        val encryptedBytes = cipher.doFinal(bytes)
        val base64EncryptedBytes = encode(encryptedBytes, android.util.Base64.DEFAULT)
        return String(base64EncryptedBytes)
    }

    fun decrypt(base64EncryptedCipherText: String): String {
        if (!_isSupported) {
            return base64EncryptedCipherText
        }
        val cipher = Cipher.getInstance(CIPHER_ALGORITHM).apply {
            init(Cipher.DECRYPT_MODE, (keyEntry as KeyStore.PrivateKeyEntry).privateKey)
        }
        val base64EncryptedBytes = base64EncryptedCipherText.toByteArray(Charsets.UTF_8)
        val encryptedBytes = decode(base64EncryptedBytes, android.util.Base64.DEFAULT)
        val decryptedBytes = cipher.doFinal(encryptedBytes)
        return String(decryptedBytes)
    }
}