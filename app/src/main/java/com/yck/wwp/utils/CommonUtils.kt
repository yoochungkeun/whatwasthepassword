package com.yck.wwp.utils

import android.Manifest
import android.R
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.util.Base64
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.yck.wwp.base.CLIP_BOARD_LABEL
import com.yck.wwp.base.PREF_SAVED_PASS_KEY
import com.yck.wwp.di.DIManager
import timber.log.Timber
import java.io.File
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*


class CommonUtils(context: Context) {

    var mContext = context
    val ITEM_DATE_TYPE = "yyyy.MM.dd-HH:mm:ss"

    fun getDate():String{
        val currentDate = (SimpleDateFormat(ITEM_DATE_TYPE)).format(Date())
        return currentDate
    }

    fun isLockNumSaved():Boolean{
        if(DIManager.repoSharedPref.getStringPref(PREF_SAVED_PASS_KEY).isNullOrEmpty()){
            return false
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.P)
    fun getSignedKey(): String? {
        var key = ""
        val packageName = mContext.packageName
        try{
            val info = mContext.packageManager.getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNING_CERTIFICATES
            )
            var signatures = info.signingInfo.apkContentsSigners
            for(signature in signatures){
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
            }
            return key
        }catch (e: Exception){
            return ""
        }
        return ""
    }

    //권한 요청
    fun requestPermissions(activity: Activity): Boolean {
        try{
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true
            }
            val permissions: Array<String> = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            ActivityCompat.requestPermissions(activity, permissions, 0)
        }catch (e: Exception){

        }
        return false
    }

    fun getExternalFilesDirUri():String{
        try{
            return mContext.getExternalFilesDir(null).toString()
        }catch (e: Exception){
            return ""
        }
    }

    //저장된 백업파일 폴더 열기
    fun openTheExportBackUpFileFolder(activity: Activity) {
        val selectedUri: Uri = Uri.parse(getExternalFilesDirUri())
        val intent = Intent(Intent.ACTION_VIEW)
        try{
            intent.setDataAndType(selectedUri, "resource/folder")
            intent.resolveActivityInfo(activity.packageManager, 0)
            activity.startActivity(intent)
        }catch (e: Exception){
            Timber.e("${e.message}")
            intent.setDataAndType(selectedUri, "*/*")
            intent.resolveActivityInfo(activity.packageManager, 0)
            activity.startActivity(intent)
        }
    }

    fun getExternalDirFileList():ArrayList<String>{
        var resultArrList = ArrayList<String>()
        if(getExternalFilesDirUri().isEmpty()) return null!!
        val dir = File(getExternalFilesDirUri())
        val files = dir.list()
        for (i in files.indices) {
            resultArrList.add(files[i].toString())
        }
        return resultArrList
    }

    fun showLoading(constl_prog_view: ConstraintLayout, activity: Activity){
        constl_prog_view.visibility = View.VISIBLE
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun hideLoading(constl_prog_view: ConstraintLayout, activity: Activity){
        constl_prog_view.visibility = View.GONE
        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    //text 클립보드 복사
    fun copyTheclipBoard(strValue: String){
        val clipboardManager: ClipboardManager = mContext.getSystemService(AppCompatActivity.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText(CLIP_BOARD_LABEL, strValue)
        clipboardManager.setPrimaryClip(clipData)
    }

}//class end