package com.yck.wwp.utils

import android.content.Context
import com.yck.wwp.di.DIManager
import timber.log.Timber
import com.yck.wwp.model.ItemsData
import com.yck.wwp.repository.RepoRmDatabase
import java.lang.Exception

class DBUtils(context:Context) {

    val mContext = context
    var mRepoRmDB: RepoRmDatabase? = null

    //instance DB
    open fun initDB(){
        if(mRepoRmDB==null)
            mRepoRmDB = RepoRmDatabase.getInstance(mContext)
    }

    //Add item
    open fun addItem(itemsData: ItemsData) {
        Timber.e("items : ${itemsData}")
        if (itemsData==null) return
        try {
            itemsData.id = DIManager.convertUtils.convertEnCryptString(itemsData.id)
            itemsData.pw = DIManager.convertUtils.convertEnCryptString(itemsData.pw)
            mRepoRmDB?.itmes_dao()?.insertItem(itemsData)
        }catch (e:Exception){
            Timber.e(e.message)
        }
    }

    //Update
    open fun updateItem(itemsData: ItemsData){
        try{
            var encrypt_id = DIManager.convertUtils.convertEnCryptString(itemsData.id)
            var encrypt_pw = DIManager.convertUtils.convertEnCryptString(itemsData.pw)
            mRepoRmDB?.itmes_dao()?.updateIdItem(idx = itemsData.idx,description = itemsData.description,id = encrypt_id,pw = encrypt_pw,date = itemsData.date,isfavorite = itemsData.isfavorite, color = itemsData.color)
        }catch (e:Exception){
            Timber.e(e.message)
        }
    }

    //getAll Items
    open fun getAllItems():MutableList<ItemsData>{
        try{
            var items = mRepoRmDB?.itmes_dao()?.getAll()
            return items!!
        }catch (e:Exception){
            Timber.e(e.message)
        }
        return null!!
    }

    //Delete Item
    open fun deleteItem(idx:Int){
        mRepoRmDB?.itmes_dao()?.deleteById(idx)
    }

    //get Item
    open fun getIdItem(idx:Int):ItemsData{
        try{
            var items = mRepoRmDB?.itmes_dao()?.getIdItem(idx)
            return items!!
        }catch (e:Exception){
            Timber.e(e.message)
        }
        return null!!
    }

    //Search Item
    open fun searchItem(keyword:String):MutableList<ItemsData>{
        if (keyword==null) return null!!
        try{
            return mRepoRmDB?.itmes_dao()?.getSearch(keyword)!!
        }catch (e:Exception){
            Timber.e(e.message)
            return null!!
        }
    }

    //Search Item & Favorite
    open fun searchIteminFavorite(keyword:String,isFavorite:String):MutableList<ItemsData>{
        if (keyword==null) return null!!
        try{
            val result = mRepoRmDB?.itmes_dao()?.getSearchinFavorite(keyword,isFavorite)
            return result!!
        }catch (e:Exception){
            Timber.e(e.message)
            return null!!
        }
    }

    open fun clearDBData(){
        try{
            mRepoRmDB?.clearAllTables()
        }catch (e:Exception){
            Timber.e(e.message)
            return null!!
        }
    }


}//class end