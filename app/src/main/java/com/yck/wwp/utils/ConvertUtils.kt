package com.yck.wwp.utils

import android.content.Context
import android.util.Log
import com.yck.wwp.base.FAVORITE_O
import com.yck.wwp.base.FAVORITE_TRUE
import com.yck.wwp.base.FAVORITE_X
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ItemsData
import java.io.File

class ConvertUtils(context: Context) {

    var mContext = context

    fun convertIsFavoriteToOX(decryptItem: ItemsData): String {
        if (decryptItem.isfavorite.equals(FAVORITE_TRUE)) {
            return FAVORITE_O
        } else {
            return FAVORITE_X
        }
    }

    fun convertStringToSecretString(value:String):String{
        var result = ""
        (0..value.toCharArray().size).forEach { i ->
            Log.d("transString","i : ${i}")
            result += "*"
        }
        return result
    }

    fun convertEnCryptString(valueString:String):String{
        DIManager.commonUtils.getSignedKey()?.let { DIManager.androidKeyStoreUtil.init(it) }
        try {
            return DIManager.androidKeyStoreUtil.encrypt(valueString)
        }catch (e:Exception){
            return ""
        }
    }

    fun convertDeCryptString(valueString:String):String{
        DIManager.commonUtils.getSignedKey()?.let { DIManager.androidKeyStoreUtil.init(it) }
        try {
            return DIManager.androidKeyStoreUtil.decrypt(valueString)
        }catch (e:Exception){
            return ""
        }
    }

    fun convertArrayListToArray(valueArrList:ArrayList<String>): Array<String?> {
        var resultArr: Array<String?> = arrayOfNulls<String>(valueArrList.size)
        try {
            for(i in 0 until valueArrList.size){
                resultArr[i] = valueArrList.get(i)
            }
            return resultArr
        }catch (e:Exception){
            return null!!
        }
    }

    //FilePath Full uri for Developer -> FilePath Full uri for User
    fun convertFilePathForUser(value_file:File): String {
        var value_str:String = ""
        try {
            value_str = value_file.toString()
            var startIdx = value_str.indexOf('0')+1
            return value_str.substring(startIdx,value_str.length)
        }catch (e:Exception){
            return ""
        }
    }

}//class end