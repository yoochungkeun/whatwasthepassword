package com.yck.wwp.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.core.content.ContextCompat
import com.yck.wwp.R
import java.lang.Exception


class ViewCommonUtils(context:Context): View(context) {

    var mContext = context
    lateinit var mGradi_drawable:GradientDrawable

    //Gradident 바탕 색상 변경
    fun setDrawableSolidColor(color_value: String,drawable_value:Int):GradientDrawable{
        mGradi_drawable = ContextCompat.getDrawable(mContext,drawable_value) as GradientDrawable
        try{
            mGradi_drawable.setColor(Color.parseColor(color_value))
        }catch (e:Exception){
            mGradi_drawable.setColor(mContext.resources.getColor(R.color.color_pick_pastel_gray))
        }
        return mGradi_drawable
    }

}//class end