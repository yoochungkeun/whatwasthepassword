package com.yck.wwp.utils

import android.app.Activity
import android.os.Environment
import com.yck.wwp.R
import com.yck.wwp.base.*
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ItemsData
import kotlinx.coroutines.GlobalScope
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException


class DBimportExportUitls() {

    lateinit var mActivity: Activity
    lateinit var mSheet: Sheet
    lateinit var mRow: Row
    val mWorkbook: Workbook = HSSFWorkbook()
    var mTitleList = emptyArray<String>()
    var mItems = ItemsData()

    constructor(activity: Activity): this(){
        mActivity = activity
    }

    //exportDataFileToExcel
    fun startExcelWork(item_list: MutableList<ItemsData>) {
        mTitleList = arrayOf(
                mActivity.resources.getString(R.string.str_business_excel_cell_A1),
                mActivity.resources.getString(R.string.str_business_excel_cell_B1),
                mActivity.resources.getString(R.string.str_business_excel_cell_C1),
                mActivity.resources.getString(R.string.str_business_excel_cell_D1),
                mActivity.resources.getString(R.string.str_business_excel_cell_E1),
                mActivity.resources.getString(R.string.str_business_excel_cell_F1),
                mActivity.resources.getString(R.string.str_business_excel_cell_G1)
        )
        //row : in Excel 1,2,3,4...
        //cell : in Excel A,B,C,D...
        mSheet = mWorkbook.createSheet()
        mRow = mSheet.createRow(0) // 새로운 행 생성
        var cell: Cell
        for (i in 0 until mTitleList.size) {
            cell = mRow.createCell(i)
            cell.setCellValue(mTitleList.get(i))
        }
        //Excel Title 생성 완료
        try {
            for (i in 0 until item_list.size) { // 데이터 엑셀에 입력
                var item = item_list[i]
                mRow = mSheet.createRow(i + 1)
                //number
                cell = mRow.createCell(0)
                cell.setCellValue("${i}")
                //description
                cell = mRow.createCell(1)
                cell.setCellValue(item.description)
                //id
                cell = mRow.createCell(2)
                cell.setCellValue(DIManager.convertUtils.convertDeCryptString(item.id))
                //pass
                cell = mRow.createCell(3)
                cell.setCellValue(DIManager.convertUtils.convertDeCryptString(item.pw))
                //date
                cell = mRow.createCell(4)
                cell.setCellValue(item.date)
                //isFavorite
                cell = mRow.createCell(5)
                cell.setCellValue(DIManager.convertUtils.convertIsFavoriteToOX(item))
                //color
                cell = mRow.createCell(6)
                cell.setCellValue(item.color)
            }
        } catch (e: Exception) {
            Timber.e("e.message : ${e.message}")
        }
    }

    //저장된 리스트 내용 백업파일 생성
    fun createExcelFile() {
        var mXlsFile = File(mActivity.getExternalFilesDir(null), "${BUISINESS_EXCEL_FILE_NAME}_${DIManager.commonUtils.getDate()}.xls")
        var getStrFilePath = File(mActivity.getExternalFilesDir(null)?.absolutePath, "${BUISINESS_EXCEL_FILE_NAME}_${DIManager.commonUtils.getDate()}.xls")
        DIManager.repoSharedPref.setStringPref(PREF_SAVED_FILE_PATH_KEY, DIManager.convertUtils.convertFilePathForUser(getStrFilePath))

        try {
            val os = FileOutputStream(mXlsFile)
            if (mWorkbook == null) return
            mWorkbook.write(os) // 내부 저장소에 엑셀 파일 생성
        } catch (e: IOException) {
            Timber.e("e.message : ${e.message}")
        }
    }


    //백업파일 -> App 으로 가져오기
    //importDataFileToExcel
    fun importBackupFile(strFilePath: String){
        DIManager.dbUtils.initDB()
        try{
            var fis = FileInputStream(strFilePath)
            var workbook = HSSFWorkbook(fis)
            var sheet = workbook.getSheetAt(0)

            var rows = sheet.physicalNumberOfRows

            for (i in 1 until rows step 1){

                var row = sheet.getRow(i)

                if(row!=null){

                    var cells = row.physicalNumberOfCells

                    for(j in 1 until cells step 1){

                        // j == 0 일때에는 excel 의 title row 이기때문에 제외함
                        var cell = row.getCell(j)

                        Timber.e("cell.stringCellValue : ${cell.stringCellValue}")

                        when(j){
                            0 -> {
                                mItems.idx = cell.columnIndex
                            }
                            1 -> {
                                mItems.description = cell.stringCellValue
                            }
                            2 -> {
                                mItems.id = cell.stringCellValue
                            }
                            3 -> {
                                mItems.pw = cell.stringCellValue
                            }
                            4 -> {
                                mItems.date = cell.stringCellValue
                            }
                            5 -> {
                                if (cell.stringCellValue.equals(FAVORITE_O)) {
                                    mItems.isfavorite = FAVORITE_TRUE
                                } else {
                                    mItems.isfavorite = FAVORITE_FALSE
                                }
                            }
                            6 -> {
                                mItems.color = cell.toString()
                            }
                        }
                        Timber.e("==== cell ====")
                    }
                    Timber.e("==== cell end ====")
                }
                Timber.e("==== row end ====")
                GlobalScope.run {
                    DIManager.dbUtils.addItem(mItems)

                }
            }

        }catch (e: Exception){
            Timber.e("e.message : ${e.message}")
        }
    }




}//class end