package com.yck.wwp.utils

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.*
import com.yck.wwp.base.FIREBASE_CLOUDSTORE_COLLECTION
import com.yck.wwp.base.FIREBASE_CLOUDSTORE_COMPLETE
import com.yck.wwp.base.FIREBASE_CLOUDSTORE_DOCUMENT
import com.yck.wwp.model.ResFirebaseCloudStoreData
import com.yck.wwp.model.ViewData
import timber.log.Timber
import java.lang.Exception

class FirebaseUtils(context: Context) {

    var mContext = context
    lateinit var mFirebaseCloud:FirebaseFirestore
    lateinit var mDocRef:DocumentReference
    var mIntroLiveData = MutableLiveData<ViewData>()
    var mIntroViewData = ViewData()


    fun startGetFirebseCloudStoreData(any:Any){
        if(mIntroLiveData is MutableLiveData<ViewData>){
            mIntroLiveData = any as MutableLiveData<ViewData>
        }
        initFirebaseInstance()
        initDocumentSnapListener()
    }

    fun initFirebaseInstance(){
        try{
            mFirebaseCloud = FirebaseFirestore.getInstance()
        }catch (e:Exception){
            Timber.e("${e.message.toString()}")
        }
    }

    fun initDocumentSnapListener(){
        try{
            mDocRef = mFirebaseCloud.collection(FIREBASE_CLOUDSTORE_COLLECTION).document(FIREBASE_CLOUDSTORE_DOCUMENT)
            mDocRef.addSnapshotListener(setDocumentSnapEvenListener())
        }catch (e:Exception){
            Timber.e("${e.message.toString()}")
        }
    }

    fun setDocumentSnapEvenListener():EventListener<DocumentSnapshot> = object :EventListener<DocumentSnapshot>{
        override fun onEvent(value: DocumentSnapshot?, error: FirebaseFirestoreException?) {
            try{
                if(error!=null || value==null || !value.exists()){
                    Timber.e("${error?.message.toString()}")
                }else{
                    mIntroViewData.resFirebaseCloud = value?.toObject(ResFirebaseCloudStoreData::class.java)!!
                }
                mIntroViewData.command = FIREBASE_CLOUDSTORE_COMPLETE
                mIntroLiveData.postValue(mIntroViewData)
            }catch (e:Exception){
                Timber.e("${e.message.toString()}")
            }
        }
    }


}//class end
