package com.yck.wwp.di

import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import com.yck.wwp.repository.RepoSharedPref
import com.yck.wwp.utils.*


object DIManager: KoinComponent {

    val dbUtils: DBUtils by inject()
    val commonUtils: CommonUtils by inject()
    val viewCommonUtils: ViewCommonUtils by inject()
    val repoSharedPref: RepoSharedPref by inject()
    val androidKeyStoreUtil: AndroidKeyStoreUtil by inject()
    val convertUtils: ConvertUtils by inject()
    val firebaseUtils: FirebaseUtils by inject()

}//class end