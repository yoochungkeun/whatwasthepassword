package ck.accountlistmanager.di

import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module
import com.yck.wwp.repository.RepoSharedPref
import com.yck.wwp.utils.*

val modules = module {
    single { DBUtils(androidContext()) }
    single { CommonUtils(androidApplication()) }
    single { ViewCommonUtils(androidApplication()) }
    single { RepoSharedPref(androidApplication()) }
    single { AndroidKeyStoreUtil(androidApplication()) }
    single { ConvertUtils(androidApplication()) }
    single { FirebaseUtils(androidApplication()) }
}

var customDImodules = listOf(
    modules
)


