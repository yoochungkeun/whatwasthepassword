package com.yck.wwp.model

import java.io.Serializable

data class ResFirebaseCloudStoreData(
        var title:String = "",
        var content:String = "",
        var btn_left:String = "",
        var btn_right:String = ""
):Serializable