package com.yck.wwp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "AccountItems")
data class ItemsData(
    @PrimaryKey(autoGenerate = true) var idx:Int = 0,
    var description:String = "",
    var id:String = "",
    var pw:String = "",
    var date:String = "",
    var isfavorite:String = "false",
    var color:String = ""
):Serializable