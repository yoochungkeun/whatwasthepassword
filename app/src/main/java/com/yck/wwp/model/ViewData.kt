package com.yck.wwp.model

import java.io.Serializable

data class ViewData(
        var is_clear_db:Boolean = false,
        var command:String = "",
        var keyword:String = "",
        var pass_command:String = "",
        var inputPass:String = "",
        var item:ItemsData = ItemsData(),
        var itemList:MutableList<ItemsData> = mutableListOf(),
        var resFirebaseCloud:ResFirebaseCloudStoreData = ResFirebaseCloudStoreData()

):Serializable