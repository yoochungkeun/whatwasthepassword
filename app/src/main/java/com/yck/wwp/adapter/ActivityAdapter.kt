package com.yck.wwp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.yck.wwp.interfaces.IOnItemClickListener
import com.yck.wwp.interfaces.IOnItemLongClickListener
import com.yck.wwp.R
import com.yck.wwp.base.FAVORITE_TRUE
import com.yck.wwp.databinding.ItemListBinding
import com.yck.wwp.di.DIManager
import com.yck.wwp.model.ItemsData


class ActivityAdapter(context: Context, val resInfoList: List<ItemsData>) :
    RecyclerView.Adapter<ActivityAdapter.Holder>() {

    var mContext = context
    private lateinit var mItemsData: ItemsData
    private lateinit var mListener: IOnItemClickListener
    private lateinit var mLongListener: IOnItemLongClickListener

    override fun getItemViewType(position: Int): Int {
        mItemsData = resInfoList[position]
        return position
    }

    override fun getItemCount(): Int {
        return resInfoList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return  Holder(ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind()
    }

    inner class Holder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        var inner_binding = binding.let {
            it
        }
        @SuppressLint("Range", "ResourceType")
        fun bind() {
            if(inner_binding==null) return

            var bindinView = (inner_binding as ItemListBinding)
            bindinView.itemsList = mItemsData
            var color = mContext.resources.getString(R.color.color_pick_pastel_gray)
            if(!mItemsData.color.isEmpty()){
                color = mItemsData.color
            }
            bindinView.ctrlBody.background = DIManager.viewCommonUtils.setDrawableSolidColor(color,R.drawable.item_layout_shape)
            bindinView.ivBookmark.visibility = View.GONE
            if(mItemsData.isfavorite.equals(FAVORITE_TRUE)){
                bindinView.ivBookmark.visibility = View.VISIBLE
            }

            val animation: Animation = AnimationUtils.loadAnimation(mContext, R.anim.click_scale_anim_for_item_list)
            bindinView.ctrlBody.setOnTouchListener(object: View.OnTouchListener{
                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                    when (event!!.action) {
                        MotionEvent.ACTION_DOWN -> {
                            bindinView.ctrlBody.startAnimation(animation)
                        }
                    }
                    return false
                }
            })

            bindinView.ctrlBody.setOnClickListener(object :View.OnClickListener{
                override fun onClick(v: View?) {
                    mListener.onItemClick(resInfoList[adapterPosition],adapterPosition)
                }
            })

            bindinView.ctrlBody.setOnLongClickListener(object :View.OnLongClickListener{
                override fun onLongClick(p0: View?): Boolean {
                    mLongListener.onItemLongClick(resInfoList[adapterPosition],adapterPosition)
                    return true
                }
            })
        }
    }

    fun setOnItemClickListener(listener: IOnItemClickListener) {
        this.mListener = listener
    }

    fun setOnItemLongLcickListener(longListener:IOnItemLongClickListener){
        this.mLongListener = longListener
    }


}//class end