package com.yck.wwp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.yck.wwp.base.TAB_FAVORITE_FRAG_PAGE
import com.yck.wwp.base.TAB_MAIN_FRAG_PAGE
import com.yck.wwp.base.TAB_MAX_ITEM_COUNT
import com.yck.wwp.view.fragment.FragmentTypeFavorite
import com.yck.wwp.view.fragment.FragmentTypeMain


class FragAdapter(fragmentActivity: FragmentActivity): FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return TAB_MAX_ITEM_COUNT
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            TAB_MAIN_FRAG_PAGE -> {
                FragmentTypeMain()
            }
            TAB_FAVORITE_FRAG_PAGE -> {
                FragmentTypeFavorite()
            }
            else -> FragmentTypeMain()
        }
    }



}//class end
